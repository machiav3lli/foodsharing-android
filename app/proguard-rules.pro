-dontobfuscate

# Keep models
-keep class de.foodsharing.api.** { *; }
-keep class de.foodsharing.model.** { *; }
-keep class de.foodsharing.notifications.** { *; }
