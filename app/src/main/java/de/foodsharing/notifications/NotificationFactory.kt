package de.foodsharing.notifications

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Icon
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.Person
import androidx.core.app.RemoteInput
import androidx.core.app.TaskStackBuilder
import androidx.core.content.ContextCompat
import androidx.core.content.getSystemService
import androidx.core.graphics.drawable.IconCompat
import com.bumptech.glide.Glide
import de.foodsharing.R
import de.foodsharing.model.User
import de.foodsharing.ui.conversation.ConversationActivity
import de.foodsharing.ui.main.MainActivity
import de.foodsharing.utils.Utils
import de.foodsharing.utils.testing.OpenForTesting
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Create and manages and system notification.
 */
@Singleton
@OpenForTesting
class NotificationFactory @Inject constructor(
    final val context: Context
) {
    private val notificationManager: NotificationManager = context.getSystemService()!!

    init {
        initDefaultChannels()
    }

    private fun initDefaultChannels() {
        // No initialization is required for pre-O devices.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return
        }

        // TODO update this
        val defaultChannel = NotificationChannel(
            DEFAULT_PUSH_MESSAGE_TYPE,
            context.getString(R.string.notification_channel_other),
            NotificationManager.IMPORTANCE_DEFAULT)
        notificationManager.createNotificationChannel(defaultChannel)

        val conversationChannel = NotificationChannel(
            CONVERSATION_PUSH_MESSAGE_TYPE,
            context.getString(R.string.notification_channel_chat),
            NotificationManager.IMPORTANCE_DEFAULT)
        conversationChannel.description = context.getString(R.string.notification_channel_chat_description)
        notificationManager.createNotificationChannel(conversationChannel)
    }

    private fun getBuilder(channelId: String): NotificationCompat.Builder {
        return NotificationCompat.Builder(context, channelId)
            .setColor(ContextCompat.getColor(context, R.color.primary500))
            .setSmallIcon(R.drawable.notification_icon)
            .setDefaults(Notification.DEFAULT_ALL)
    }

    fun createNotification(message: PushMessage) {
        when (message) {
            is ConversationPushMessage -> {
                addMessageToConversationNotification(
                    message.conversationId,
                    message.message.body,
                    message.message.sentAt.time,
                    message.author
                )
            }
            is DefaultPushMessage -> {
                createDefaultNotification(message)
            }
        }
    }

    fun addMessageToConversationNotification(conversationId: Int, text: String, timestamp: Long, author: User) {
        // TODO handle error
        val picture = Glide.with(context).asBitmap().load(Utils.getUserPhotoURL(author)).submit()

        if (Build.VERSION.SDK_INT >= 28) {
            // Filter out notification by `notificationId`
            val statusBarNotification = notificationManager.activeNotifications.firstOrNull {
                it.id == conversationId
            }

            statusBarNotification?.notification?.let { notification ->
                // Add new `Message` to original `Notification.Builder`
                val recoveredNotificationBuilder = Notification.Builder.recoverBuilder(context, notification)
                recoveredNotificationBuilder.also {
                    val messageStyle = it.style as Notification.MessagingStyle
                    val updateAuthor = android.app.Person.Builder()
                        .setKey(author.id.toString())
                        .setName(author.name)
                        .setIcon(Icon.createWithBitmap(picture.get()))
                        .build()
                    messageStyle.addMessage(text, timestamp, updateAuthor)
                    it.style = messageStyle
                }

                notificationManager.notify(conversationId, recoveredNotificationBuilder.build())
                return
            }
        }

        val authorPerson = Person.Builder()
            .setKey(author.id.toString())
            .setName(author.name)
            .setIcon(IconCompat.createWithBitmap(picture.get()))
            .build()

        val youLabel = context.getString(R.string.you)
        val person = Person.Builder().setName(youLabel).build()
        val style = NotificationCompat.MessagingStyle(person)
            .addMessage(text, timestamp, authorPerson)

        val replyLabel = context.getString(R.string.reply)
        val remoteInput: RemoteInput = RemoteInput.Builder(ConversationReplyBroadcastReceiver.KEY_TEXT_REPLY)
            .setLabel(replyLabel)
            .build()

        val intent = Intent(context, ConversationReplyBroadcastReceiver::class.java).also {
            it.putExtra(ConversationReplyBroadcastReceiver.EXTRA_CONVERSATION_ID, conversationId)
        }
        val replyPendingIntent: PendingIntent =
            PendingIntent.getBroadcast(context,
                conversationId,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT)

        val action: NotificationCompat.Action =
            NotificationCompat.Action.Builder(R.drawable.ic_reply, replyLabel, replyPendingIntent)
                .addRemoteInput(remoteInput)
                .setAllowGeneratedReplies(false)
                .setSemanticAction(NotificationCompat.Action.SEMANTIC_ACTION_REPLY)
                .build()

        val openConversationIntent = Intent(context, ConversationActivity::class.java)
        openConversationIntent.putExtra(ConversationActivity.EXTRA_CONVERSATION_ID, conversationId)
        val fullPendingIntent: PendingIntent? = TaskStackBuilder.create(context).run {
            addNextIntentWithParentStack(openConversationIntent)
            getPendingIntent(0, PendingIntent.FLAG_ONE_SHOT)
        }

        val notification = getBuilder(CONVERSATION_PUSH_MESSAGE_TYPE)
            .setStyle(style)
            .addAction(action)
            .setContentIntent(fullPendingIntent)
            .setAutoCancel(true)
            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
            .build()

        notificationManager.notify(conversationId, notification)
    }

    private fun createDefaultNotification(message: DefaultPushMessage) {
        val builder = getBuilder(message.type)
        val intent = Intent(context, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            context, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val notificationId = message.hashCode()
        val notification = builder.setContentTitle(message.caption)
            .setContentText(message.body)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .build()

        notificationManager.notify(notificationId, notification)
    }

    fun removeConversationNotification(conversationId: Int) {
        notificationManager.cancel(conversationId)
    }
}
