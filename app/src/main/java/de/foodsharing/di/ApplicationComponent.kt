package de.foodsharing.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import de.foodsharing.FoodsharingApplication
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AndroidSupportInjectionModule::class,
            ActivitiesModule::class,
            FlavorActivitiesModule::class,
            ApplicationModule::class,
            FragmentsModule::class,
            ViewModelModule::class
        ]
)
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: FoodsharingApplication): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: FoodsharingApplication)
}
