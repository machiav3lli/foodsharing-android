package de.foodsharing.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.baskets.MyBasketsFragment
import de.foodsharing.ui.baskets.NearbyBasketsFragment
import de.foodsharing.ui.conversations.ConversationsFragment
import de.foodsharing.ui.fsp.NearbyFoodSharePointsFragment
import de.foodsharing.ui.main.GiveFragment
import de.foodsharing.ui.main.TakeFragment
import de.foodsharing.ui.map.BaseMapFragment
import de.foodsharing.ui.map.MapFragment
import de.foodsharing.ui.pickup.PickupSlotFragment
import de.foodsharing.ui.pickups.FuturePickupsFragment
import de.foodsharing.ui.posts.PostsFragment

@Module
abstract class FragmentsModule {

    @ContributesAndroidInjector
    abstract fun bindBaseFragment(): BaseFragment

    @ContributesAndroidInjector
    abstract fun bindBaseMapFragment(): BaseMapFragment

    @ContributesAndroidInjector
    abstract fun bindConversationListFragment(): ConversationsFragment

    @ContributesAndroidInjector
    abstract fun bindMapFragment(): MapFragment

    @ContributesAndroidInjector
    abstract fun bindNearbyBasketListFragment(): NearbyBasketsFragment

    @ContributesAndroidInjector
    abstract fun bindMyBasketListFragment(): MyBasketsFragment

    @ContributesAndroidInjector
    abstract fun bindPostsFragment(): PostsFragment

    @ContributesAndroidInjector
    abstract fun bindNearbyFoodSharePointsFragment(): NearbyFoodSharePointsFragment

    @ContributesAndroidInjector
    abstract fun bindGiveFragment(): GiveFragment

    @ContributesAndroidInjector
    abstract fun bindTakeFragment(): TakeFragment

    @ContributesAndroidInjector
    abstract fun bindFuturePickupsFragment(): FuturePickupsFragment

    @ContributesAndroidInjector
    abstract fun bindPickupSlotFragment(): PickupSlotFragment
}
