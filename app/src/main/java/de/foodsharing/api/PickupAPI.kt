package de.foodsharing.api

import de.foodsharing.model.StorePickup
import de.foodsharing.model.UserPickup
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface PickupAPI {

    /**
     * Returns all future pickups for which the user is registered.
     */
    @GET("/api/pickup/registered")
    fun getRegisteredPickups(): Observable<List<UserPickup>>

    class StorePickupsResponse {

        var pickups: List<StorePickup>? = null
    }
    /**
     * Returns future pickups in a store.
     */
    @GET("/api/stores/{id}/pickups")
    fun getPickupsForStore(@Path("id") storeId: Int): Observable<StorePickupsResponse>
}
