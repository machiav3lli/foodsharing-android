package de.foodsharing.api

import de.foodsharing.model.ConversationDetailResponse
import de.foodsharing.model.ConversationIdResponse
import de.foodsharing.model.ConversationListResponse
import de.foodsharing.model.ConversationMessagesResponse
import io.reactivex.Completable
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Retrofit API interface for chat messages.
 */
interface ConversationsAPI {
    /**
     * Requests a list of the user's conversations.
     */
    @GET("/api/conversations")
    fun list(
        @Query("limit") number: Int,
        @Query("offset") offset: Int = 0
    ): Observable<ConversationListResponse>

    /**
     * Requests details and messages of a conversation.
     * @param id conversation id
     * @param number how many messages to return
     */
    @GET("/api/conversations/{id}")
    fun get(
        @Path("id") id: Int,
        @Query("messagesLimit") number: Int
    ): Observable<ConversationDetailResponse>

    /**
     * Requests messages of a conversation.
     * @param id conversation id
     * @param olderThanId id of oldest already known message
     * @param limit number of messages to return
     */
    @GET("/api/conversations/{id}/messages")
    fun getMessages(
        @Path("id") id: Int,
        @Query("olderThanId") olderThanId: Int,
        @Query("limit") limit: Int
    ): Observable<ConversationMessagesResponse>

    /**
     * Attempts to send a message to all members of a conversation.
     * @param id conversation id
     * @param body the message
     */
    @FormUrlEncoded
    @POST("/api/conversations/{id}/messages")
    fun send(
        @Path("id") id: Int,
        @Field("body") body: String
    ): Completable

    /**
     * Returns or creates a conversation id with the given foodsharer id.
     * @param userId id of the foodsharer
     */
    @GET("/api/user/{userId}/conversation")
    fun user2conv(
        @Path("userId") userId: Int
    ): Observable<ConversationIdResponse>
}
