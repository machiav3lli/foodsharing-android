package de.foodsharing.model

import com.google.gson.annotations.SerializedName
import java.util.Date

data class FoodSharePoint(
    val id: Int,
    @SerializedName("bid")
    val regionId: Int,
    val name: String,
    val description: String,
    val address: String,
    val city: String,
    val postcode: String,
    val createdAt: Date,
    val picture: String?,
    override val lat: Double,
    override val lon: Double
) : ICoordinate
