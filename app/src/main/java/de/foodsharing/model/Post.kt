package de.foodsharing.model

import com.google.gson.annotations.SerializedName
import de.foodsharing.utils.BASE_URL
import java.util.Date

data class PostPicture(
    private val image: String,
    private val medium: String,
    private val thumb: String
) {
    fun thumbSizeUrl() = "$BASE_URL/$thumb"
    fun mediumSizeUrl() = "$BASE_URL/$medium"
    fun fullSizeUrl() = "$BASE_URL/$image"
}

data class Post(
    val id: Int,
    val body: String,
    @SerializedName("createdAt") // needed for fields with a camel case name
    val createdAt: Date,
    val pictures: List<PostPicture>?,
    val author: User
)

data class SendPostResponse(val post: Post)

data class PostsResponse(
    @SerializedName("mayPost")
    val mayPost: Boolean,
    @SerializedName("mayDelete")
    val mayDelete: Boolean,
    val results: List<Post>
)
