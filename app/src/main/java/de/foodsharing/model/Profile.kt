package de.foodsharing.model

/**
 * Represents a person that is participating in foodsharing. In contrast to [User] this includes
 * all details about a user.
 */
data class Profile(
    val id: Int,
    val firstname: String,
    val lastname: String,
    val address: String?,
    val city: String?,
    val postcode: String?,
    val coordinates: Coordinate?,
    val email: String,
    val landline: String?,
    val mobile: String?
)
