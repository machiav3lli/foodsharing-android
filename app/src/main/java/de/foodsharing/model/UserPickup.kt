package de.foodsharing.model

import java.util.Date

data class UserPickup(
    val date: Date,
    val store: Store,
    // TODO: change into a boolean in the backend
    val confirmed: Int,
    val slots: UserPickupSlotsList
)

data class UserPickupSlotsList(
    val max: Int,
    val occupied: List<UserPickupSlotProfile>
)

/**
 * Represents a person that is signed in to a pickup slot.
 */
data class UserPickupSlotProfile(
    val id: Int,
    val name: String?,
    val avatar: String?,
    val confirmed: Int
)
