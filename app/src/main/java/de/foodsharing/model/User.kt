package de.foodsharing.model

import java.io.Serializable

/**
 * Represents a person that is participating in foodsharing.
 * If and only if the name of users is null, they deleted their account.
 */
data class User(
    val id: Int,
    val name: String?,
    val avatar: String?,
    val sleepStatus: Int?
) : Serializable
