package de.foodsharing.utils

import android.content.Context
import android.content.Intent
import dagger.Lazy
import de.foodsharing.services.AuthService
import de.foodsharing.ui.login.LoginActivity
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UnauthorizedRedirectInterceptor @Inject constructor(
    private val context: Context,
    private val auth: Lazy<AuthService>
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder().build()
        val response = chain.proceed(request)
        if (response.code() == 401) {
            auth.get().clear()
            val intent = Intent(context, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }
        return response
    }
}
