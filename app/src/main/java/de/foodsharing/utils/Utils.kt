package de.foodsharing.utils

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.content.ComponentName
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.Intent.EXTRA_SUBJECT
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Parcelable
import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat
import androidx.core.content.getSystemService
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.createBitmap
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.net.toUri
import androidx.exifinterface.media.ExifInterface
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.BuildConfig
import de.foodsharing.R
import de.foodsharing.model.Coordinate
import de.foodsharing.model.Popup
import de.foodsharing.model.User
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt

object Utils {

    /**
     * Creates a [Snackbar] via [Snackbar.make] and sets its background color. This function does not
     * call show() on the snackbar.
     *
     * @param view The view to find a parent from.
     * @param text The text to show. Can be formatted text.
     * @param backgroundColor The background color.
     * @param duration How long to display the message. Either [Snackbar.LENGTH_SHORT] or
     * [Snackbar.LENGTH_LONG].
     *
     * @return the created Snackbar
     */
    fun createSnackBar(view: View, text: String, backgroundColor: Int, duration: Int): Snackbar {
        return Snackbar.make(
            view,
            text,
            duration
        ).withColor(backgroundColor)
    }

    /**
     * Shows a yes-no-dialog with a specific message and calls the result function afterwards.
     *
     * @param useYesNo whether to use yes/no instead of OK/cancel for the buttons
     */
    fun showQuestionDialog(context: Context, message: String, useYesNo: Boolean = false, result: (Boolean) -> Unit) {
        val listener = DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
                result(which == DialogInterface.BUTTON_POSITIVE)
        }

        AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(if (useYesNo) R.string.yes else android.R.string.ok, listener)
                .setNegativeButton(if (useYesNo) R.string.no else android.R.string.cancel, listener)
                .show()
    }

    enum class PhotoType(val prefix: String) {
        NORMAL(""), CROP("crop_"), THUMB_CROP("thumb_crop_"),
        Q_130("130_q_"), MINI("mini_q_")
    }

    /**
     * Returns the URL of a user's profile photo.
     *
     * There are two types of URLs. Files that were uploaded with the old API need a valid [PhotoType]. For
     * files that were uploaded with the new API, the width and height parameters are used instead. This
     * function differentiates between the two types and should always be called with valid values for all
     * parameters.
     */
    fun getUserPhotoURL(user: User, type: PhotoType = PhotoType.Q_130, width: Int = 130, height: Int = width): String {
        return if (user.avatar != null && user.avatar.isNotEmpty()) {
            if (user.avatar.startsWith("/api/uploads/"))
                BASE_URL + user.avatar + "?w=$width&h=$height"
            else
                "$BASE_URL/images/${type.prefix}${user.avatar}"
        } else DEFAULT_USER_PICTURE
    }

    /**
     * Returns the URL of a header image of a food-share-point.
     *
     * There are two types of URLs. Files that were uploaded with the old API have a fixed size. For files
     * that were uploaded with the new API, the width and height parameters are used instead. This function
     * differentiates between the two types and should always be called with valid values for all parameters.
     */
    fun getFoodSharePointPhotoURL(picture: String, width: Int, height: Int): String {
        return if (picture.startsWith("/api/uploads/"))
            "$BASE_URL$picture?w=$width&h=$height"
        else
            "$BASE_URL/images/$picture"
    }

    enum class BasketPhotoType(val prefix: String) {
        NORMAL(""), MEDIUM("medium-"), THUMB("thumb-"), SMALL_75("75x75-"), SMALL_50("50x50-")
    }

    /**
     * Returns the URL of the photo that was added to a food basket.
     */
    fun getBasketPhotoURL(
        picture: String,
        type: BasketPhotoType = BasketPhotoType.NORMAL
    ) = "$BASE_URL/images/basket/${type.prefix}$picture"

    /**
     * Creates a temporary file in the external cache directory (see [Context.getExternalCacheDir])
     * and returns the file handle.
     */
    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    fun createTmpFile(context: Context, suffix: String? = null): File {
        val directory = File(context.externalCacheDir, ANDROID_CACHE_SUBDIR)
        if (!directory.exists()) directory.mkdirs()
        clearCache(directory)

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val sfx = if (suffix.isNullOrBlank()) "tmp" else suffix

        return File.createTempFile(
                "${sfx.toUpperCase(Locale.getDefault())}_${timeStamp}_",
                ".$sfx", directory
        )
    }

    /**
     * Deletes all files in the cache directory that are older than [MAX_CACHE_DURATION_DAYS] days.
     */
    private fun clearCache(directory: File) {
        directory.listFiles()?.let {
            for (file in it) {
                if (TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis() - file.lastModified())
                    > MAX_CACHE_DURATION_DAYS
                )
                    file.delete()
            }
        }
    }

    /**
     * Loads an image from a file, scales and flips it according to the exif data, scales the image
     * down to maxSize and finally returns the image as a jpeg compressed ByteArray
     */
    fun prepareImageFileForUpload(file: File, maxSize: Int = 800): ByteArray {
        val input = file.inputStream()
        val exif = ExifInterface(input)
        val isFlipped = exif.isFlipped
        val rotation = exif.rotationDegrees

        val bitmap = BitmapFactory.decodeStream(file.inputStream())
        val matrix = Matrix()
        if (isFlipped) {
            matrix.postScale(-1.0f, 1.0f)
        }
        matrix.postRotate(rotation.toFloat())
        val ratio = (maxSize.toFloat() / bitmap.width).coerceAtMost(maxSize.toFloat() / bitmap.height)
        if (ratio < 1.0f) {
            // Scale down the image if it is larger the maxSize
            matrix.postScale(ratio, ratio)
        }

        val newBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        val stream = ByteArrayOutputStream()
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream)
        return stream.toByteArray()
    }

    /**
     * Opens the given URL in a browser, if one is installed, and excludes this app from the intent
     * list. Returns whether a browser could be found.
     */
    fun openUrlInBrowser(context: Context, url: String): Boolean {
        val intent = Intent(Intent.ACTION_VIEW)
        val uri = url.toUri()
        intent.data = uri

        val shareIntentsLists = mutableListOf<Intent>()
        context.packageManager.queryIntentActivities(intent, 0).filter {
            "foodsharing" !in it.activityInfo.packageName.toLowerCase(Locale.getDefault())
        }.forEach {
            shareIntentsLists += Intent().apply {
                component = ComponentName(
                    it.activityInfo.packageName,
                    it.activityInfo.name
                )
                action = Intent.ACTION_VIEW
                data = uri
            }
        }

        if (shareIntentsLists.isEmpty()) {
            return false
        }

        val chooserIntent = Intent.createChooser(
            shareIntentsLists.removeAt(0),
            context.getString(R.string.browser_chooser_title)
        ).apply {
            putExtra(Intent.EXTRA_INITIAL_INTENTS, shareIntentsLists.toTypedArray<Parcelable>())
        }

        context.startActivity(chooserIntent)
        return true
    }

    fun openSupportEmail(context: Context, @StringRes subject: Int) {
        val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", context.getString(R.string.support_email), null))
        intent.putExtra(EXTRA_SUBJECT, context.getString(
            R.string.support_email_subject,
            BuildConfig.VERSION_NAME,
            context.getString(subject)
        ))
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.send_email_title)))
    }

    fun openShareDialog(
        context: Context,
        text: String,
        @StringRes titleId: Int = R.string.share_dialog_title
    ) {
        val intent = ShareCompat.IntentBuilder(context)
                .setType("text/plain")
                .setText(text)
                .intent
        context.startActivity(Intent.createChooser(intent, context.getString(titleId)))
    }

    fun copyToClipboard(context: Context, text: String) {
        val clipboard = context.getSystemService<ClipboardManager>()!!
        val clip = ClipData.newPlainText(null, text)
        clipboard.setPrimaryClip(clip)
    }

    /**
     * Opens an activity chooser for apps that can handle coordinates.
     */
    fun openCoordinate(context: Context, coordinate: Coordinate, label: String = "") {
        // See https://developer.android.com/guide/components/intents-common#Maps
        val uri = "geo:0,0?q=${coordinate.lat},${coordinate.lon}($label)".toUri()
        val intent = Intent(Intent.ACTION_VIEW, uri)
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.open_location_title)))
    }

    /**
     * Compares a given [integer] to a [pattern]. The [pattern] is number optionally prefixed with
     * the following comparision operators: <, >, <=, >= and =.
     */
    fun compareIntToPattern(integer: Int, pattern: String): Boolean {
        val match = Regex("([><])?(=?)(\\d+)").matchEntire(pattern)
        return if (match != null) {
            val patternNumber = match.groupValues[3].toInt()
            if (match.groupValues[2].isNotEmpty() && patternNumber == integer) {
                true
            } else if (match.groupValues[1] == "<") {
                integer < patternNumber
            } else if (match.groupValues[1] == ">") {
                integer > patternNumber
            } else if (match.groupValues[1].isEmpty() && match.groupValues[2].isEmpty()) {
                patternNumber == integer
            } else {
                false
            }
        } else {
            false
        }
    }

    fun handlePopup(context: Context, popups: List<Popup>) {
        val popup = popups.find { it.isSatisfied(context.resources) }

        if (popup != null) {
            var dialog = AlertDialog.Builder(context)
                .setMessage(popup.body)
                .setTitle(popup.title)

            if (popup.positiveAction != null) {
                dialog = dialog.setPositiveButton(popup.positiveAction.title) { _, _ ->
                    popup.positiveAction.url?.let {
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = it.toUri()
                        context.startActivity(intent)
                    }
                }
            }

            if (popup.negativeAction != null) {
                dialog = dialog.setNegativeButton(popup.negativeAction.title, null)
            }

            dialog.show()
        }
    }

    private fun getBitmapFromDrawable(drawable: Drawable): Bitmap {
        val bitmap = createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    fun getBitmapFromResourceId(context: Context, resourceId: Int): Bitmap? {
        val drawable = ContextCompat.getDrawable(context, resourceId)?.let {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                (DrawableCompat.wrap(it)).mutate()
            } else {
                it
            }
        } ?: return null

        return getBitmapFromDrawable(drawable)
    }

    private fun getBitmapFromText(
        context: Context,
        fontRes: Int,
        colorRes: Int,
        faIconRes: Int,
        textSize: Float
    ): Bitmap {
        val fontAwesomeType = ResourcesCompat.getFont(context, fontRes)
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.textSize = textSize
        paint.color = ContextCompat.getColor(context, colorRes)
        paint.textAlign = Paint.Align.LEFT
        paint.typeface = fontAwesomeType

        val baseline = -paint.ascent()
        val width = (paint.measureText(context.resources.getString(faIconRes)))
        val height = (baseline + paint.descent())
        val bitmap = createBitmap(width.toInt(), height.toInt())
        val canvas = Canvas(bitmap)
        canvas.drawText(context.resources.getString(faIconRes), 0F, baseline, paint)

        return bitmap
    }

    /**
     * Creates a drawable map marker with the specified background colour and text. In order to use an
     * icon, the text needs to be the ID of a fa-icon.
     *
     * @param colorRes resource ID of the background colour
     * @param textRes resource ID of the text
     */
    private fun getMarkerIcon(context: Context, colorRes: Int, textRes: Int): BitmapDrawable {
        val markerBitmap = getBitmapFromText(context, R.font.fa_solid_900, colorRes, R.string.fa_map_marker_solid, 100F)
        val iconBitmap = getBitmapFromText(context, R.font.fa_solid_900, android.R.color.white, textRes, 35F)

        val canvas = Canvas(markerBitmap)
        val iconPadding = (markerBitmap.width - iconBitmap.width) / 2F
        canvas.drawBitmap(iconBitmap, iconPadding, iconPadding, null)

        return BitmapDrawable(context.resources, markerBitmap)
    }

    fun getBasketMarkerIcon(context: Context) =
        getMarkerIcon(context, R.color.basketMarkerColor, R.string.fa_shopping_basket)

    fun getFspMarkerIcon(context: Context) =
        getMarkerIcon(context, R.color.fspMarkerColor, R.string.fa_recycle_solid)

    fun getCommunityMarkerIcon(context: Context) =
        getMarkerIcon(context, R.color.communityMarkerColor, R.string.fa_users)

    fun getBasketMarkerIconBitmap(context: Context) = getBitmapFromDrawable(getBasketMarkerIcon(context))

    fun getFspMakerIconBitmap(context: Context) = getBitmapFromDrawable(getFspMarkerIcon(context))

    /**
     * Formats a distance in meters into a string.
     */
    fun formatDistance(distance: Double) =
        if (distance < 1000) "${distance.roundToInt()} M" else "%.1f KM".format(distance / 1000)
}
