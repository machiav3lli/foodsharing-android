package de.foodsharing.utils

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.SystemClock
import androidx.core.content.getSystemService
import java.util.concurrent.TimeUnit

class LocationFinder private constructor() {

    companion object {
        val instance = LocationFinder()

        private const val MIN_ACCURACY = 100
        private val MAX_LOCATION_AGE_NANOS = TimeUnit.NANOSECONDS.convert(1, TimeUnit.MINUTES)
        private val MAX_LOCATION_AGE_MILLS = TimeUnit.MILLISECONDS.convert(1, TimeUnit.MINUTES)
    }

    private var locationManager: LocationManager? = null

    fun initialise(context: Context) {
        locationManager = context.getSystemService()
    }

    fun isLocationAvailable() = locationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER) == true ||
                locationManager?.isProviderEnabled(LocationManager.NETWORK_PROVIDER) == true

    fun requestLocation(callback: (Location) -> Unit): () -> Unit {
        // This is only called in cases where the permission was already checked
        @SuppressLint("MissingPermission")
        val lastLocation = getLastKnownLocationIfEnabled(LocationManager.GPS_PROVIDER)
                ?: getLastKnownLocationIfEnabled(LocationManager.NETWORK_PROVIDER)
        lastLocation?.let {
            callback(it)
        }

        val listener: LocationListener = object : LocationListener {
            override fun onLocationChanged(location: Location) {
                if (lastLocation == null ||
                        // do not downgrade to network provider again
                        !(lastLocation.provider == LocationManager.GPS_PROVIDER &&
                                location.provider == LocationManager.NETWORK_PROVIDER)) {
                    callback(location)
                }
                if (location.provider == LocationManager.GPS_PROVIDER) {
                    // Do not receive additional NETWORK PROVIDER result
                    locationManager?.removeUpdates(this)
                }
            }

            override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
            }

            override fun onProviderEnabled(provider: String) {
            }

            override fun onProviderDisabled(provider: String) {
            }
        }

        val requestLocationUpdate = updateCriteria(lastLocation)

        if (requestLocationUpdate) {
            locationManager?.apply {
                try {
                    lastLocation?.let {
                        if (it.provider == (LocationManager.GPS_PROVIDER)) {
                            requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MAX_LOCATION_AGE_MILLS,
                                MIN_ACCURACY.toFloat(),
                                listener
                            )
                        } else if (it.provider == (LocationManager.NETWORK_PROVIDER)) {
                            requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                MAX_LOCATION_AGE_MILLS,
                                MIN_ACCURACY.toFloat(),
                                listener)
                        }
                    }
                } catch (e: SecurityException) {
                    captureException(Exception("Unhandled Security exception in LocationFinder", e))
                }
            }
        }

        return {
            locationManager?.removeUpdates(listener)
        }
    }

    private fun updateCriteria(lastLocation: Location?): Boolean {

        return lastLocation == null ||
                SystemClock.elapsedRealtimeNanos() - lastLocation.elapsedRealtimeNanos > MAX_LOCATION_AGE_NANOS ||
                lastLocation.accuracy < MIN_ACCURACY
    }

    @SuppressLint("MissingPermission")
    private fun getLastKnownLocationIfEnabled(providerName: String): Location? {
        if (locationManager?.isProviderEnabled(providerName) == true) {
            return locationManager?.getLastKnownLocation(providerName)
        }
        return null
    }
}
