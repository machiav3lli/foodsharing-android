package de.foodsharing.utils

import de.foodsharing.BuildConfig

/** The base URL of the API */
const val BASE_URL = BuildConfig.BASE_URL
const val LINK_BASE_URL = BuildConfig.LINK_BASE_URL

const val LOG_TAG = "foodsharing"

const val DEFAULT_USER_PICTURE = "$BASE_URL/img/130_q_avatar.png"

const val PICTURE_FORMAT = "jpg"
const val PICTURE_MIME_TYPE = "image/jpg"

const val ONE_MEGABYTE = 1024 * 1024L
/** Size of the Http cache, set to 10 MB */
const val CACHE_SIZE = 10 * ONE_MEGABYTE
const val MAX_CACHE_DURATION_DAYS = 7
const val ANDROID_CACHE_SUBDIR = "upload_images"

const val DEFAULT_MAP_ZOOM = 12.0
const val BASKET_MAP_ZOOM = 13.0
const val DETAIL_MAP_ZOOM = 17.0
const val MAX_MAP_ZOOM = 19.0
const val CLUSTER_CLICK_ZOOM = 16.0

const val CONVERSATIONS_PER_REQUEST = 20
const val MESSAGES_PER_REQUEST = 20

const val POPUP_CONFIG_ENDPOINT = "https://gitlab.com/foodsharing-dev/foodsharing-android/raw/master/popup_config.json"

const val DEFAULT_FSP_DISTANCE_KM = 10
