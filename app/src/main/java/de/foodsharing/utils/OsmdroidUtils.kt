package de.foodsharing.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.net.ConnectivityManager
import android.view.ViewTreeObserver
import android.widget.ImageView
import androidx.core.content.getSystemService
import androidx.core.net.ConnectivityManagerCompat
import de.foodsharing.BuildConfig
import de.foodsharing.model.Coordinate
import io.reactivex.Completable
import org.osmdroid.api.IGeoPoint
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.MapTileProviderBasic
import org.osmdroid.tileprovider.tilesource.ITileSource
import org.osmdroid.tileprovider.tilesource.XYTileSource
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.MapView
import org.osmdroid.views.Projection
import org.osmdroid.views.drawing.MapSnapshot
import org.osmdroid.views.overlay.CopyrightOverlay
import java.io.File
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.Locale

object OsmdroidUtils {
    fun initialise(context: Context) {
        Configuration.getInstance().osmdroidBasePath = File(context.externalCacheDir, "osmdroid")
        Configuration.getInstance().userAgentValue =
            "Foodsharing Android/${BuildConfig.VERSION_CODE} (it@foodsharing.network)"
    }

    private fun getTileSource(context: Context, scale: Boolean): ITileSource {
        val density = if (scale) {
            context.resources.displayMetrics.density
        } else {
            1.0f
        }
        return getTileSource(density)
    }

    private fun getTileSource(deviceScale: Float): ITileSource {
        val possibleScales = arrayOf(1.0, 1.3, 1.5, 2.0, 2.6, 3.0)
        val scale = possibleScales.filter { it > deviceScale - 0.1 }.minOrNull() ?: possibleScales.maxOrNull()!!
        val df = DecimalFormat("0.#", DecimalFormatSymbols.getInstance(Locale.US))
        val mapScale = if (scale > 1.1) {
            "@${df.format(scale)}x"
        } else ""
        return XYTileSource(
                "Wikimedia", 0, MAX_MAP_ZOOM.toInt(), (256 * scale).toInt(), "$mapScale.png",
                arrayOf("https://maps.wikimedia.org/osm-intl/"),
                "Wikimedia Maps | Map data © OpenStreetMap contributors"
        )
    }

    fun setupMapView(mapView: MapView, allowHighResolution: Boolean) {
        val connectivityManager = mapView.context.getSystemService<ConnectivityManager>()!!
        val useHdMap = allowHighResolution && !ConnectivityManagerCompat.isActiveNetworkMetered(connectivityManager)

        mapView.setTileSource(getTileSource(mapView.context, useHdMap))

        // Scales tiles up according to DPI, tiles become pixelated but readable
        mapView.isTilesScaledToDpi = !useHdMap

        // add default controls
        mapView.zoomController.setVisibility(CustomZoomButtonsController.Visibility.SHOW_AND_FADEOUT)
        mapView.setMultiTouchControls(true)

        mapView.setZoomRounding(false)
        mapView.maxZoomLevel = MAX_MAP_ZOOM

        mapView.isVerticalMapRepetitionEnabled = false

        mapView.overlays.add(CopyrightOverlay(mapView.context))
    }

    fun loadMapTileToImageView(
        imageView: ImageView,
        coordinate: Coordinate,
        zoom: Double,
        marker: Bitmap?,
        allowHighResolution: Boolean
    ): () -> Unit {
        val connectivityManager =
            imageView.context.getSystemService<ConnectivityManager>()!!
        val useHdMap = allowHighResolution && !ConnectivityManagerCompat.isActiveNetworkMetered(connectivityManager)

        val tileProvider = MapTileProviderBasic(imageView.context)
        tileProvider.setOfflineFirst(true)
        tileProvider.tileSource = getTileSource(imageView.context, useHdMap)
        tileProvider.createTileCache()

        var mapSnapshot: MapSnapshot? = null

        val run = {
            mapSnapshot = MapSnapshot(MapSnapshot.MapSnapshotable {
                if (it.status != MapSnapshot.Status.CANVAS_OK) {
                    return@MapSnapshotable
                }

                val map = it.bitmap

                if (marker != null) {
                    val canvas = Canvas(it.bitmap)
                    val paint = Paint()
                    canvas.drawBitmap(
                            marker,
                            map.width / 2.0f - marker.width / 2.0f,
                            map.height / 2.0f - marker.height,
                            paint
                    )
                }

                imageView.setImageBitmap(it.bitmap)
                it.onDetach()
                tileProvider.detach()
            }, MapSnapshot.INCLUDE_FLAG_UPTODATE, tileProvider, emptyList(),
                    Projection(zoom, imageView.width, imageView.height, coordinate.toGeoPoint(), 0f, true, true, 0, 0)
            )
            Completable.fromRunnable(mapSnapshot).subscribe()
        }

        // The height or width of the image view might still be zero at this point. If this is the
        // case wait for the layout to complete before initializing the snapshot.
        if (imageView.width > 0 && imageView.height > 0) {
            run()
        } else {
            imageView.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    if (imageView.width > 0 && imageView.height > 0) {
                        imageView.viewTreeObserver.removeOnPreDrawListener(this)
                        run()
                    }
                    return false
                }
            })
        }

        return {
            if (mapSnapshot?.status != MapSnapshot.Status.CANVAS_OK) {
                mapSnapshot?.onDetach()
            }
        }
    }

    fun IGeoPoint.toCoordinate(): Coordinate {
        return Coordinate(latitude, longitude)
    }

    fun Coordinate.toGeoPoint(): GeoPoint {
        return GeoPoint(lat, lon)
    }
}
