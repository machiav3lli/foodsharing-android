package de.foodsharing.ui.conversations

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.ChangeBounds
import androidx.transition.Slide
import androidx.transition.TransitionManager
import androidx.transition.TransitionSet
import com.google.android.material.snackbar.Snackbar
import com.stfalcon.chatkit.dialogs.DialogsListAdapter
import de.foodsharing.BuildConfig
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.conversation.ChatkitMessage
import de.foodsharing.ui.conversation.ConversationActivity
import de.foodsharing.utils.ChatkitGlideImageLoader
import de.foodsharing.utils.withColor
import kotlinx.android.synthetic.main.fragment_conversation_list.*
import kotlinx.android.synthetic.main.fragment_conversation_list.view.*
import javax.inject.Inject

class ConversationsFragment : BaseFragment(),
        DialogsListAdapter.OnDialogClickListener<ChatkitConversation>,
        Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: ConversationsViewModel by viewModels { viewModelFactory }

    private lateinit var adapter: FsDialogListAdapter
    private var errorSnackbar: Snackbar? = null

    // Number of items at the end of the list before starting to load the next page
    private val NUM_PREFETCH_ITEMS = 10

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_conversation_list, container, false)

        val layoutManager = LinearLayoutManager(activity)
        view.recycler_view.layoutManager = layoutManager
        adapter = FsDialogListAdapter(ChatkitGlideImageLoader(requireContext()))
        adapter.setOnDialogClickListener(this)
        adapter.setDatesFormatter(ConversationDateFormatter(requireContext()))
        view.recycler_view.setAdapter(adapter)

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            view.recycler_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }

        view.pull_refresh.setOnRefreshListener {
            viewModel.refresh()
        }

        // listener that notices if the user scrolled to the bottom of the conversations list
        view.recycler_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                val linLayoutManager = view.recycler_view.layoutManager as? LinearLayoutManager
                val remainingItems = adapter.itemCount - (linLayoutManager?.findLastVisibleItemPosition() ?: -1)
                if (remainingItems < NUM_PREFETCH_ITEMS) {
                    viewModel.loadNext()
                }
            }
        })

        view.no_conversations_label.movementMethod = LinkMovementMethod.getInstance()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        bindViewModel()
        handlePushNotificationSnackbar()
    }

    class BannerTransition : TransitionSet() {
        init {
            init()
        }

        private fun init() {
            ordering = ORDERING_TOGETHER

            addTransition(ChangeBounds())
                .addTransition(Slide(Gravity.TOP))
        }
    }

    private fun handlePushNotificationSnackbar() {
        if (BuildConfig.FLAVOR == "play" && preferences.pushNotificationsEnabled == null) {
            banner.visibility = View.VISIBLE
            button_push_later.setOnClickListener {
                TransitionManager.beginDelayedTransition(inner_container, BannerTransition())
                banner.visibility = View.GONE
                preferences.pushNotificationsEnabled = false
            }

            button_push_activate.setOnClickListener {
                TransitionManager.beginDelayedTransition(inner_container, BannerTransition())
                banner.visibility = View.GONE
                preferences.pushNotificationsEnabled = true
            }
        } else {
            banner.visibility = View.GONE
        }
    }

    private fun bindViewModel() {
        val diff = AsyncListDiffer(adapter, object : DiffUtil.ItemCallback<ChatkitConversation>() {
            override fun areItemsTheSame(oldItem: ChatkitConversation, newItem: ChatkitConversation): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: ChatkitConversation,
                newItem: ChatkitConversation
            ): Boolean {
                return oldItem == newItem
            }
        })

        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                val linearLayoutManager = recycler_view.layoutManager as? LinearLayoutManager
                val firstPosition = linearLayoutManager?.findFirstVisibleItemPosition()
                if (positionStart == 0 && firstPosition == 0) {
                    linearLayoutManager.scrollToPositionWithOffset(0, 0)
                }
            }

            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                val linearLayoutManager = recycler_view.layoutManager as? LinearLayoutManager
                val firstPosition = linearLayoutManager?.findFirstVisibleItemPosition()
                if ((toPosition == 0 || fromPosition == 0) && firstPosition == 0) {
                    linearLayoutManager.scrollToPositionWithOffset(0, 0)
                }
            }
        })

        viewModel.conversationsWithCurrentUser.observe(viewLifecycleOwner, Observer {
            val conversations = it.first ?: return@Observer
            val currentUser = it.second ?: return@Observer

            val updatedList = conversations.map { c ->
                ChatkitConversation(c, c.lastMessage?.let { m ->
                    ChatkitMessage(m, m.author)
                }, currentUser.id)
            }

            diff.submitList(updatedList) {
                adapter.setItemsWithoutNotify(diff.currentList)
            }

            no_conversations_label.visibility = if (conversations.isEmpty()) {
                View.VISIBLE
            } else {
                View.GONE
            }

            recycler_view.visibility = if (conversations.isEmpty()) {
                View.GONE
            } else {
                View.VISIBLE
            }
        })

        viewModel.isLoading.observe(viewLifecycleOwner) {
            progress_bar.visibility = if (it) View.VISIBLE else View.GONE
        }

        viewModel.isReloading.observe(viewLifecycleOwner) {
            pull_refresh.isRefreshing = it
        }

        viewModel.errorState.observe(viewLifecycleOwner) {
            errorSnackbar?.dismiss()
            val rootView = view
            val fragmentContext = context
            if (it != null && rootView != null && fragmentContext != null) {
                errorSnackbar = Snackbar.make(
                    rootView,
                    it,
                    Snackbar.LENGTH_INDEFINITE
                )
                    .setAction(R.string.retry_button) {
                        viewModel.tryAgain()
                    }.setActionTextColor(ContextCompat.getColor(fragmentContext, R.color.white))
                    .withColor(ContextCompat.getColor(fragmentContext, R.color.colorSecondary))
                errorSnackbar?.show()
            }
        }
    }

    override fun onDialogClick(conversation: ChatkitConversation) {
        // show conversation
        var message: String? = null
        if (activity?.intent?.action == Intent.ACTION_SEND && activity?.intent?.type == "text/plain") {
            message = activity?.intent?.getStringExtra(Intent.EXTRA_TEXT)
        }
        ConversationActivity.start(requireContext(), conversation.conversation.id, message)
        activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        adapter.setOnDialogClickListener(null)
        errorSnackbar?.dismiss()
        errorSnackbar = null
    }
}
