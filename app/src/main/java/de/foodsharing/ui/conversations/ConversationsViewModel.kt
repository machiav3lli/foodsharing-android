package de.foodsharing.ui.conversations

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.model.Conversation
import de.foodsharing.model.User
import de.foodsharing.services.AuthService
import de.foodsharing.services.ConversationsService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.utils.captureException
import de.foodsharing.utils.combineWith
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class ConversationsViewModel @Inject constructor(
    private val conversationsService: ConversationsService,
    authService: AuthService
) : BaseViewModel() {
    private val loadNextEvents = PublishSubject.create<Any>()
    private val reloadEvents = PublishSubject.create<Any>()
    private val tryAgainEvents = PublishSubject.create<Any>()

    val conversations = MutableLiveData<List<Conversation>>()
    val currentUser = MutableLiveData<User>()

    val conversationsWithCurrentUser: LiveData<Pair<List<Conversation>?, User?>> =
        conversations.combineWith(currentUser) { list, user -> Pair(list, user) }

    private val isLoadingCurrentUser = MutableLiveData<Boolean>()
    private val isLoadingConversations = MutableLiveData<Boolean>()

    val isReloading = MutableLiveData<Boolean>()
    val errorState = MutableLiveData<Int>()

    val isLoading = isLoadingConversations
        .combineWith(isLoadingCurrentUser) { a, b -> a?.or(b ?: return@combineWith null) }
        .combineWith(errorState) { loading, errorState ->
            loading?.and(errorState == null) ?: false
        }

    init {
        initConversationLoading()

        isLoadingCurrentUser.postValue(true)
        request(authService.currentUser().retryWhen(getErrorHandler(isLoadingCurrentUser)), {
            isLoadingCurrentUser.postValue(false)
            currentUser.postValue(it)
        }, {
            // Network errors should be handled by retryWhen
            captureException(Exception("Unexpected error while loading current user.", it))
        })
    }

    private fun initConversationLoading() {
        isLoadingConversations.value = true
        isReloading.value = false
        request(conversationsService.listPaged(reloadEvents.doOnNext {
            isReloading.postValue(true)
        }, loadNextEvents.doOnNext {
            isLoadingConversations.postValue(true)
        }, getErrorHandler(isLoadingConversations)), {
            conversations.postValue(it)

            isLoadingConversations.postValue(false)
            isReloading.postValue(false)

            errorState.postValue(null)
        }, {
            // Expected errors should be handled by the error handler
            captureException(Exception("Unexpected error while loading conversations.", it))
        })
    }

    private fun getErrorHandler(loadingIndicator: MutableLiveData<Boolean>): ((
        Observable<Throwable>
    ) -> ObservableSource<Any>) = {
        it.flatMap { error ->
            if (error is IOException) {
                // Network Error
                errorState.postValue(R.string.error_no_connection)
            } else if (error is HttpException && error.code() == 401) {
                errorState.postValue(R.string.error_no_connection)
            } else {
                // HttpException or unknown exception
                // Should not happen
                errorState.postValue(R.string.error_unknown)
                captureException(Exception("Unexpected exception in ConversationsViewModel error handler", error))
            }
            loadingIndicator.postValue(false)
            isReloading.postValue(false)
            tryAgainEvents.doOnNext {
                errorState.postValue(null)
                loadingIndicator.postValue(true)
            }
        }
    }

    fun loadNext() {
        loadNextEvents.onNext(true)
    }

    fun refresh() {
        reloadEvents.onNext(true)
    }

    fun tryAgain() {
        tryAgainEvents.onNext(true)
    }
}
