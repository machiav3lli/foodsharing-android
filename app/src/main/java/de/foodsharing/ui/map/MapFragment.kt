package de.foodsharing.ui.map

import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.model.CommunityMarker
import de.foodsharing.model.Coordinate
import de.foodsharing.model.FoodSharePoint
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.basket.BasketActivity
import de.foodsharing.ui.community.CommunityActivity
import de.foodsharing.ui.fsp.FoodSharePointActivity
import de.foodsharing.utils.LOG_TAG
import de.foodsharing.utils.Utils.getBasketMarkerIcon
import de.foodsharing.utils.Utils.getCommunityMarkerIcon
import de.foodsharing.utils.Utils.getFspMarkerIcon
import kotlinx.android.synthetic.main.fragment_map.view.map_layers_button
import org.osmdroid.bonuspack.clustering.RadiusMarkerClusterer
import org.osmdroid.util.BoundingBox
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.Marker
import javax.inject.Inject

class MapFragment : BaseMapFragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: MapViewModel by viewModels { viewModelFactory }

    private lateinit var markerOverlay: RadiusMarkerClusterer
    private var basketMarkers: List<Marker>? = null
    private var fspMarkers: List<Marker>? = null
    private var communityMarkers: List<Marker>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        if (!super.canRestoreMapCamera(savedInstanceState)) {
            // try to restore old camera view or zoom out to germany
            if (!restoreMapCamera()) {
                val initialBoundingBox = BoundingBox.fromGeoPoints(
                        listOf(GeoPoint(46.0, 4.0), GeoPoint(55.0, 17.0)))
                mapView.addOnFirstLayoutListener { _, _, _, _, _ ->
                    mapView.zoomToBoundingBox(initialBoundingBox, false)
                }
            }
        }

        context?.let {
            markerOverlay = FsRadiusMarkerClusterer(it).apply {
                onClusterClickListener = this@MapFragment::onClusterClick
            }
            markerOverlay.setRadius((it.resources.displayMetrics.density * 72).toInt())
            markerOverlay.setMaxClusteringZoomLevel(Integer.MAX_VALUE)

            mapView.overlays.add(markerOverlay)
        }

        setupLayersButton(view)

        bindViewModel()

        return view
    }

    private fun setupLayersButton(view: View?) {
        val layersButton = view?.map_layers_button ?: return

        val contextThemeWrapper = ContextThemeWrapper(context, R.style.PopupMenuOverlapAnchor)
        val popup = PopupMenu(contextThemeWrapper, layersButton, Gravity.TOP or Gravity.END)
        popup.menuInflater.inflate(R.menu.layer_menu, popup.menu)

        popup.menu.findItem(R.id.basket_layer_item).isChecked = viewModel.isShowingBaskets.value == true
        popup.menu.findItem(R.id.fsp_layer_item).isChecked = viewModel.isShowingFSPs.value == true
        popup.menu.findItem(R.id.community_layer_item).isChecked = viewModel.isShowingCommunities.value == true

        popup.setOnMenuItemClickListener { item ->
            item.isChecked = !item.isChecked
            when (item.itemId) {
                R.id.basket_layer_item -> viewModel.isShowingBaskets.value = item.isChecked
                R.id.fsp_layer_item -> viewModel.isShowingFSPs.value = item.isChecked
                R.id.community_layer_item -> viewModel.isShowingCommunities.value = item.isChecked
            }
            updateMarkers()
            false
        }

        layersButton.setOnClickListener {
            context?.let {
                popup.show()
            }
        }
    }

    private fun bindViewModel() {
        viewModel.showError.observe(viewLifecycleOwner, EventObserver {
            showMessage(getString(it))
        })

        viewModel.markers.observe(viewLifecycleOwner) {
            setMarkers(
                it?.first ?: emptyList(),
                it?.second ?: emptyList(),
                it?.third ?: emptyList()
            )
        }

        viewModel.isShowingBaskets.observe(viewLifecycleOwner) {
            updateMarkers()
        }
        viewModel.isShowingFSPs.observe(viewLifecycleOwner) {
            updateMarkers()
        }
        viewModel.isShowingCommunities.observe(viewLifecycleOwner) {
            updateMarkers()
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_map
    }

    private fun restoreMapCamera(): Boolean {
        updateLocation(
            preferences.mapCenterCoordinate ?: return false,
            preferences.mapZoom,
            false
        )
        return true
    }

    private fun storeMapCamera() {
        preferences.mapCenterCoordinate = Coordinate(
            mapView.boundingBox.centerLatitude,
            mapView.boundingBox.centerLongitude
        )
        preferences.mapZoom = mapView.zoomLevelDouble
    }

    private fun setMarkers(
        foodSharePoints: List<FoodSharePoint>,
        baskets: List<Basket>,
        communities: List<CommunityMarker>
    ) {
        // add food-share-point markers
        val fspIcon = getFspMarkerIcon(requireContext())
        fspMarkers = foodSharePoints.map { fsp ->
            val marker = Marker(mapView)
            marker.id = fsp.id.toString()
            marker.position = GeoPoint(fsp.lat, fsp.lon)
            marker.icon = fspIcon
            marker.relatedObject = fsp
            marker.setOnMarkerClickListener { m, _ ->
                val foodsp = m.relatedObject as FoodSharePoint
                Log.v(LOG_TAG, "$foodsp")

                // show fsp details in an activity
                FoodSharePointActivity.start(requireContext(), foodsp.id)
                activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                true
            }
            marker
        }

        val basketIcon = getBasketMarkerIcon(requireContext())
        basketMarkers = baskets.map { b ->
            val marker = Marker(mapView)
            marker.id = b.id.toString()
            marker.position = GeoPoint(b.lat, b.lon)
            marker.icon = basketIcon
            marker.relatedObject = b
            marker.setOnMarkerClickListener { m, _ ->
                val basket = m.relatedObject as Basket
                Log.v(LOG_TAG, "$basket")

                // show basket details in an activity
                BasketActivity.start(requireContext(), basket.id)
                activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                true
            }
            marker
        }

        val communityIcon = getCommunityMarkerIcon(requireContext())
        communityMarkers = communities.map { c ->
            val marker = Marker(mapView)
            marker.id = c.id.toString()
            marker.position = GeoPoint(c.lat, c.lon)
            marker.icon = communityIcon
            marker.relatedObject = c
            marker.setOnMarkerClickListener { m, _ ->
                val community = m.relatedObject as CommunityMarker
                Log.v(LOG_TAG, "$community")

                // show community description in an activity
                CommunityActivity.start(requireContext(), community.id)
                activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                true
            }
            marker
        }

        updateMarkers()
    }

    private fun onClusterClick(markers: List<Marker>) {
        val ctx = context ?: return
        val builder = AlertDialog.Builder(ctx)

        val arrayAdapter = ArrayAdapter<String>(ctx, android.R.layout.select_dialog_item)
        markers.map {
            when (val relatedObj = it.relatedObject) {
                is Basket -> "${getString(R.string.basket_label)} #${relatedObj.id}"
                is FoodSharePoint -> "${getString(R.string.foodsharepoint_label)} #${relatedObj.id}"
                is CommunityMarker -> "${getString(R.string.community_label)} #${relatedObj.id}"
                else -> getString(R.string.unknown)
            }
        }.forEach { arrayAdapter.add(it) }

        builder.setNegativeButton(android.R.string.cancel, null)

        builder.setAdapter(arrayAdapter) { _, index ->
            when (val relatedObj = markers[index].relatedObject) {
                is Basket -> BasketActivity.start(requireContext(), relatedObj.id)
                is FoodSharePoint -> FoodSharePointActivity.start(requireContext(), relatedObj.id)
                // is CommunityMarker -> CommunityActivity.start(requireContext(), relatedObj.regionId)
                else -> return@setAdapter
            }
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }
        builder.show()
    }

    private fun updateMarkers() {
        markerOverlay.items.clear()

        if (viewModel.isShowingBaskets.value == true) {
            basketMarkers?.forEach {
                markerOverlay.add(it)
            }
        }
        if (viewModel.isShowingFSPs.value == true) {
            fspMarkers?.forEach {
                markerOverlay.add(it)
            }
        }
        if (viewModel.isShowingCommunities.value == true) {
            communityMarkers?.forEach {
                markerOverlay.add(it)
            }
        }

        markerOverlay.invalidate()
        mapView.invalidate()
    }

    override fun onDestroyView() {
        storeMapCamera()
        super.onDestroyView()
    }
}
