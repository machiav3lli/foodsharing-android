package de.foodsharing.ui.profile

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.activity.viewModels
import androidx.appcompat.widget.TooltipCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.api.PostsAPI
import de.foodsharing.di.Injectable
import de.foodsharing.model.User
import de.foodsharing.services.PreferenceManager
import de.foodsharing.ui.base.AuthRequiredBaseActivity
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.conversation.ConversationActivity
import de.foodsharing.ui.posts.PostsFragment
import de.foodsharing.utils.LINK_BASE_URL
import de.foodsharing.utils.Utils
import de.foodsharing.utils.getDisplayName
import kotlinx.android.synthetic.main.activity_profile.profile_content_view
import kotlinx.android.synthetic.main.activity_profile.profile_message_button
import kotlinx.android.synthetic.main.activity_profile.profile_name
import kotlinx.android.synthetic.main.activity_profile.profile_picture
import kotlinx.android.synthetic.main.activity_profile.profile_posts_fragment
import kotlinx.android.synthetic.main.activity_profile.progress_bar
import kotlinx.android.synthetic.main.activity_profile.toolbar
import javax.inject.Inject

class ProfileActivity : AuthRequiredBaseActivity(), Injectable {

    companion object {
        private const val EXTRA_USER = "user"
        private const val EXTRA_USER_ID = "id"
        private const val PROFILE_URL = "$LINK_BASE_URL/profile/%d"

        fun start(context: Context, user: User) {
            val extras = bundleOf(EXTRA_USER to user)
            val intent = Intent(context, ProfileActivity::class.java).putExtras(extras)
            context.startActivity(intent)
        }

        fun start(context: Context, userId: Int) {
            val extras = bundleOf(EXTRA_USER_ID to userId)
            val intent = Intent(context, ProfileActivity::class.java).putExtras(extras)
            context.startActivity(intent)
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var preferenceManager: PreferenceManager

    private val profileViewModel: ProfileViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rootLayoutID = R.id.profile_content
        setContentView(R.layout.activity_profile)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        bindViewModel()

        if (intent.hasExtra(EXTRA_USER)) {
            val user = intent.getSerializableExtra(EXTRA_USER) as User
            profileViewModel.profile.value = user
            profileViewModel.userId = user.id
            display(user)
        } else {
            val id = intent.getIntExtra(EXTRA_USER_ID, -1)
            supportActionBar?.title = "${getString(R.string.profile_title)}: #$id"
            profileViewModel.userId = id
        }
    }

    private fun bindViewModel() {
        profileViewModel.isCurrentUser.observe(this) { isCurrentUser ->
            if (isCurrentUser) {
                profile_message_button.visibility = GONE
            } else {
                profile_message_button.setOnClickListener {
                    ConversationActivity.start(this, profileViewModel.userId!!)
                }
                profile_message_button.visibility = VISIBLE
            }
        }

        profileViewModel.isLoading.observe(this) {
            progress_bar.visibility = if (it) VISIBLE else GONE
        }

        profileViewModel.showError.observe(this, EventObserver {
            showErrorMessage(getString(it))
        })

        profileViewModel.profile.observe(this) {
            display(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.profile_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.profile_open_website_button -> {
            getProfileUrl()?.let {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(it))
                startActivity(browserIntent)
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun getProfileUrl(): String? = profileViewModel.userId?.let { PROFILE_URL.format(it) }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    private fun display(user: User) {
        supportActionBar?.title = user.getDisplayName(this)
        user.getDisplayName(this).let {
            profile_name.text = it
            (profile_posts_fragment as PostsFragment).setTarget(PostsAPI.Target.PROFILE, user.id,
                label = getString(R.string.status_updates_by_user, it))
        }
        TooltipCompat.setTooltipText(profile_name, "ID: ${user.id}")
        TooltipCompat.setTooltipText(profile_picture, "ID: ${user.id}")

        var photoQuality = Utils.PhotoType.NORMAL
        var width = 389
        var height = 500
        if (preferenceManager.useLowResolutionImages) {
            photoQuality = Utils.PhotoType.Q_130
            width /= 2
            height /= 2
        }
        Glide.with(this)
            .load(Utils.getUserPhotoURL(user, photoQuality, width, height))
            .fitCenter()
            .centerCrop()
            .error(R.drawable.default_user_picture)
            .into(profile_picture)

        profile_content_view.visibility = VISIBLE
        progress_bar.visibility = GONE
    }

    private fun showErrorMessage(error: String) {
        progress_bar.visibility = GONE
        showMessage(error, duration = Snackbar.LENGTH_LONG)
    }
}
