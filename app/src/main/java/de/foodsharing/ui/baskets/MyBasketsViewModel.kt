package de.foodsharing.ui.baskets

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.model.Basket
import de.foodsharing.services.BasketService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class MyBasketsViewModel @Inject constructor(
    private val basketService: BasketService
) : BaseViewModel() {

    val baskets = MutableLiveData<List<Basket>>()

    val isLoading = MutableLiveData<Boolean>().apply {
        value = true
    }
    val showError = MutableLiveData<Event<Int>>()

    private val refreshEvents = BehaviorSubject.createDefault<Any>(true)

    init {
        request(refreshEvents.doOnNext {
            isLoading.postValue(true)
        }.switchMap {
            getAllCurrentUsersBaskets().subscribeOn(Schedulers.io())
        }, {
            isLoading.value = false
            baskets.value = it
        }, {
            isLoading.value = false
            showError.value = Event(R.string.error_unknown)
        })
    }

    private fun getAllCurrentUsersBaskets() = basketService.list().map { basketResponse ->
        basketResponse.baskets?.sortedByDescending { basket ->
            basket.createdAt
        } ?: emptyList()
    }

    fun reload() {
        refreshEvents.onNext(true)
    }
}
