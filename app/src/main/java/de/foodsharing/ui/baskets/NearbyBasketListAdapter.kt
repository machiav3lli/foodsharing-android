package de.foodsharing.ui.baskets

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import de.foodsharing.R
import de.foodsharing.model.Basket
import de.foodsharing.utils.BASE_URL
import de.foodsharing.utils.CustomDateFormatter
import de.foodsharing.utils.Utils
import de.foodsharing.utils.getDisplayName
import de.foodsharing.utils.inflate
import kotlinx.android.synthetic.main.item_basket.view.item_description
import kotlinx.android.synthetic.main.item_basket.view.item_distance
import kotlinx.android.synthetic.main.item_basket.view.item_picture
import kotlinx.android.synthetic.main.item_basket.view.item_title_text

// TODO: combine with MyBasketListAdapter
class NearbyBasketListAdapter(
    private val onClickListener: (Basket) -> Unit,
    private val formatter: CustomDateFormatter,
    val context: Context
) : RecyclerView.Adapter<NearbyBasketListAdapter.BasketHolder>() {
    private var baskets: List<BasketItemModel> = emptyList()

    fun setBaskets(newBaskets: List<BasketItemModel>) {
        val diffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return baskets[oldItemPosition].basket.id == newBaskets[newItemPosition].basket.id
            }

            override fun getOldListSize(): Int {
                return baskets.size
            }

            override fun getNewListSize(): Int {
                return newBaskets.size
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return baskets[oldItemPosition].basket == newBaskets[newItemPosition].basket
            }
        })

        baskets = newBaskets
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemCount(): Int = baskets.size

    override fun onBindViewHolder(holder: BasketHolder, position: Int) {
        if (position in 0 until itemCount) {
            holder.bind(baskets[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): BasketHolder {
        return BasketHolder(
                parent.inflate(
                        R.layout.item_basket,
                        false
                ), onClickListener, formatter, context
        )
    }

    class BasketHolder(
        val view: View,
        private val onClickListener: (Basket) -> Unit,
        private val formatter: CustomDateFormatter,
        val context: Context
    ) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private var basket: Basket? = null

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            basket?.let {
                onClickListener(it)
            }
        }

        fun bind(item: BasketItemModel) {
            val basket = item.basket
            this.basket = basket

            // view.item_creation_date.text = formatter.format(basket.createdAt)
            view.item_description.text = basket.description

            if (!basket.picture.isNullOrEmpty()) {
                view.item_picture.visibility = View.VISIBLE
                val pictureUrl = "$BASE_URL/images/basket/${basket.picture}"
                Glide.with(context)
                        .load(pictureUrl)
                        .apply(RequestOptions().transform(CenterCrop(), RoundedCorners(8)))
                        .error(R.drawable.basket_default_picture)
                        .into(view.item_picture)
            } else {
                view.item_picture.visibility = View.GONE
            }

            view.item_title_text.text =
                context.getString(R.string.basket_title, basket.creator.getDisplayName(context)).toUpperCase()

            if (item.distance != null) {
                view.item_distance.visibility = View.VISIBLE
                view.item_distance.text = Utils.formatDistance(item.distance)
            } else {
                view.item_distance.visibility = View.GONE
            }
        }
    }
}
