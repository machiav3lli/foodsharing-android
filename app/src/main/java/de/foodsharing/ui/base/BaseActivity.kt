package de.foodsharing.ui.base

import android.annotation.SuppressLint
import android.content.Context
import android.content.IntentFilter
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.os.LocaleList
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.services.PreferenceManager
import de.foodsharing.utils.ConnectivityReceiver
import de.foodsharing.utils.Utils
import java.util.Locale
import javax.inject.Inject

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity(), ConnectivityReceiver.ConnectivityReceiverListener, Injectable {

    private var snackBar: Snackbar? = null
    private var connectionSnackBar: Snackbar? = null
    private lateinit var locale: Locale
    var rootLayoutID: Int? = null
    var isConnected = false

    @Inject
    lateinit var preferences: PreferenceManager

    private val connectivityReceiver = ConnectivityReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registerReceiver(
                connectivityReceiver,
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }

    override fun attachBaseContext(newBase: Context) {
        val prefs = androidx.preference.PreferenceManager.getDefaultSharedPreferences(newBase)
        locale = PreferenceManager(prefs, newBase).language
        applyOverrideConfiguration(getLocalizedConfiguration(locale))
        super.attachBaseContext(newBase)
    }

    /**
     * Creates a new configuration with the given locale.
     */
    private fun getLocalizedConfiguration(locale: Locale): Configuration {
        val config = Configuration()
        return config.apply {
            config.setLayoutDirection(locale)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                config.setLocale(locale)
                val localeList = LocaleList(locale)
                LocaleList.setDefault(localeList)
                config.setLocales(localeList)
            } else {
                config.setLocale(locale)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        try {
            unregisterReceiver(connectivityReceiver)
        } catch (e: Exception) {
            /* This happens if the activity was restarted before the receiver was successfully registered.
            It can be ignored */
        }
        ConnectivityReceiver.connectivityReceiverListener = null
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(
                connectivityReceiver,
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
        ConnectivityReceiver.connectivityReceiverListener = this

        // restart the activty to update if the locale has changed
        val prefs = androidx.preference.PreferenceManager.getDefaultSharedPreferences(this)
        val currentLocaleCode = PreferenceManager(prefs, this).language
        if (locale != currentLocaleCode) {
            recreate()
            locale = currentLocaleCode
        }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        toggleConnectionMessage(isConnected)
        this.isConnected = isConnected
    }

    fun showMessage(message: String, duration: Int = Snackbar.LENGTH_INDEFINITE) {
        rootLayoutID?.let {
            snackBar = getSnackbar(it, message, duration)
            snackBar?.show()
        }
    }

    private fun toggleConnectionMessage(isConnected: Boolean) {
        if (!isConnected) {
            val messageToUser = getString(R.string.offline_message)
            onDisconnectedListener?.onDisconnected()
            rootLayoutID?.let {
                connectionSnackBar = getSnackbar(it, messageToUser, Snackbar.LENGTH_INDEFINITE)
                connectionSnackBar?.show()
            }
        } else {
            connectionSnackBar?.dismiss()
            onConnectedListener?.onConnectionAvailable()
        }
    }

    private fun getSnackbar(rootLayoutID: Int, messageToUser: String, duration: Int): Snackbar =
            Utils.createSnackBar(
                    findViewById(rootLayoutID),
                    messageToUser,
                    ContextCompat.getColor(this, R.color.colorSecondary),
                    duration
            )

    interface OnConnectionAvailableListener {
        fun onConnectionAvailable()
    }

    interface OnDisconnectedListener {
        fun onDisconnected()
    }

    companion object {
        var onConnectedListener: OnConnectionAvailableListener? = null
        var onDisconnectedListener: OnDisconnectedListener? = null
    }
}
