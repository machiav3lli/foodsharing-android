package de.foodsharing.ui.base

import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LiveData
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.utils.withColor

internal class ErrorStateRetrySnackbar(
    val lifecycle: Lifecycle,
    val errorState: LiveData<Int>,
    val retryHandler: () -> Unit,
    val context: Context,
    val rootView: View
) {
    private var errorSnackbar: Snackbar? = null

    init {
        errorState.observe({
            lifecycle
        }, {
            errorSnackbar?.dismiss()
            if (it != null) {
                errorSnackbar = Snackbar.make(
                    rootView,
                    it,
                    Snackbar.LENGTH_INDEFINITE
                )
                    .setAction(R.string.retry_button) {
                        retryHandler()
                    }
                    .setActionTextColor(ContextCompat.getColor(context, R.color.white))
                    .setBehavior(object : BaseTransientBottomBar.Behavior() {
                        override fun canSwipeDismissView(child: View): Boolean {
                            return false
                        }
                    })
                    .withColor(ContextCompat.getColor(context, R.color.colorSecondary))
                errorSnackbar?.show()
            }
        })
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroy() {
        errorSnackbar?.dismiss()
        errorSnackbar = null
    }
}
