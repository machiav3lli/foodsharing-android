package de.foodsharing.ui.fsp

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.api.FoodSharePointAPI
import de.foodsharing.model.FoodSharePoint
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import retrofit2.HttpException
import javax.inject.Inject

class FoodSharePointViewModel @Inject constructor(
    private val fspAPI: FoodSharePointAPI
) : BaseViewModel() {
    var foodSharePointId: Int? = null
        set(value) {
            if (field != value) {
                field = value
                fetch()
            }
        }

    val isLoading = MutableLiveData<Boolean>().apply { value = true }
    val showError = MutableLiveData<Event<Int>>()
    val foodSharePoint = MutableLiveData<FoodSharePoint>()

    private fun fetch() {
        val id = foodSharePointId
        if (id != null) {
            request(fspAPI.get(id).doOnNext {
                isLoading.postValue(true)
            }, {
                isLoading.value = false
                foodSharePoint.value = it
            }, {
                isLoading.value = false

                val stringRes = if (it is HttpException && it.code() == 404) {
                    R.string.foodsharepoint_404
                } else {
                    R.string.error_unknown
                }
                showError.value = Event(stringRes)
            })
        } else {
            foodSharePoint.value = null
        }
    }
}
