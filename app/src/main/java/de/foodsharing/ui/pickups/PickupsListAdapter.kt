package de.foodsharing.ui.pickups

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.foodsharing.R
import de.foodsharing.model.UserPickup
import de.foodsharing.utils.inflate
import kotlinx.android.synthetic.main.item_future_pickup.view.item_date
import kotlinx.android.synthetic.main.item_future_pickup.view.item_store_name

class PickupsListAdapter(
    private val onClickListener: (UserPickup) -> Unit,
    val context: Context
) : RecyclerView.Adapter<PickupsListAdapter.PickupHolder>() {
    private var pickups = emptyList<UserPickup>()
    var dateFormatter = FuturePickupDateFormatter(context)

    override fun getItemCount() = pickups.size

    override fun onBindViewHolder(holder: PickupHolder, position: Int) {
        if (position in 0 until itemCount) {
            holder.bind(pickups[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): PickupHolder {
        return PickupHolder(parent.inflate(R.layout.item_future_pickup, false), onClickListener, context)
    }

    fun setPickups(pickups: List<UserPickup>) {
        this.pickups = pickups
        notifyDataSetChanged()
    }

    inner class PickupHolder(
        val view: View,
        private val onClickListener: (UserPickup) -> Unit,
        val context: Context
    ) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private var pickup: UserPickup? = null

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            pickup?.let {
                onClickListener(it)
            }
        }

        fun bind(pickup: UserPickup) {
            this.pickup = pickup

            view.item_date.text = dateFormatter.format(pickup.date)
            view.item_store_name.text = pickup.store.name
        }
    }
}
