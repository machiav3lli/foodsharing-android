package de.foodsharing.ui.pickups

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import de.foodsharing.R
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.pickup.PickupActivity
import de.foodsharing.utils.NonScrollingLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_future_pickups.no_future_pickups_label
import kotlinx.android.synthetic.main.fragment_future_pickups.progress_bar
import kotlinx.android.synthetic.main.fragment_future_pickups.recycler_view
import kotlinx.android.synthetic.main.fragment_future_pickups.view.recycler_view
import javax.inject.Inject

class FuturePickupsFragment : BaseFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: PickupsViewModel by viewModels { viewModelFactory }

    private lateinit var adapter: PickupsListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_future_pickups, container, false)
        view.recycler_view.layoutManager = NonScrollingLinearLayoutManager(activity)

        adapter = PickupsListAdapter({ pickup ->
            PickupActivity.start(requireContext(), pickup.store, pickup.date)
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }, requireContext())
        view.recycler_view.adapter = adapter

        bindViewModel()

        return view
    }

    private fun bindViewModel() {
        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it) {
                no_future_pickups_label.visibility = View.GONE
                progress_bar.visibility = View.VISIBLE
            } else {
                no_future_pickups_label.visibility = View.VISIBLE
                progress_bar.visibility = View.GONE
            }
        }

        viewModel.showError.observe(viewLifecycleOwner, EventObserver {
            showMessage(getString(it))
        })

        viewModel.pickups.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                recycler_view.visibility = View.GONE
                no_future_pickups_label.visibility = View.VISIBLE
            } else {
                recycler_view.visibility = View.VISIBLE
                no_future_pickups_label.visibility = View.GONE

                adapter.setPickups(it)
            }
        }
    }

    fun reload() {
        viewModel.reload()
    }
}
