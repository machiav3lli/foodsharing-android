package de.foodsharing.ui.pickups

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.model.UserPickup
import de.foodsharing.services.PickupService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class PickupsViewModel @Inject constructor(
    private val pickupsService: PickupService
) : BaseViewModel() {

    // status and output data
    val pickups = MutableLiveData<List<UserPickup>>()
    val isLoading = MutableLiveData<Boolean>().apply {
        value = true
    }
    val showError = MutableLiveData<Event<Int>>()

    private val refreshEvents = BehaviorSubject.createDefault<Any>(true)

    init {
        request(refreshEvents.doOnNext {
            isLoading.postValue(true)
        }.switchMap {
            pickupsService.getRegisteredPickups().subscribeOn(Schedulers.io())
        }, {
            isLoading.value = false
            pickups.value = it
        }, {
            isLoading.value = false
            showError.value = Event(R.string.error_unknown)
        })
    }

    fun reload() {
        refreshEvents.onNext(true)
    }
}
