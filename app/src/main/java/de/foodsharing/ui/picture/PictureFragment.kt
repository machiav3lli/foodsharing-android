package de.foodsharing.ui.picture

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import de.foodsharing.BuildConfig
import de.foodsharing.R
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.base.Event
import de.foodsharing.ui.picture.PictureActivity.Companion.EXTRA_PICTURE_FILE
import de.foodsharing.utils.PICTURE_FORMAT
import de.foodsharing.utils.Utils
import de.foodsharing.utils.captureException
import kotlinx.android.synthetic.main.fragment_picture.add_picture_button
import kotlinx.android.synthetic.main.fragment_picture.picture_view
import kotlinx.android.synthetic.main.fragment_picture.view.add_picture_button
import kotlinx.android.synthetic.main.fragment_picture.view.picture_view
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

/**
 * A UI snippet that displays a picture and handles the possibility to take a photo. The embedding
 * activity should be an instance of [BaseActivity].
 */
class PictureFragment : Fragment() {

    companion object {
        private const val STATE_PICTURE_FILE = "picture_file"
        private const val STATE_REQUESTED_FILE = "requested_file"
    }

    // The current image
    var file: File? = null
        set(value) {
            field = value
            showFile()
        }
    var pictureUri: String? = null
        set(value) {
            field = value
            showPictureFromUri()
        }
    /**
     * A resource that is used as a default picture if no file or URI is specified.
     */
    var defaultPicture: Int = R.drawable.basket_default_picture
    // A temporary file where the captured image is written to
    private var requestedFile: File? = null

    val pictureWasChanged = MutableLiveData<Event<File?>>()
    private var showImageLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_CANCELED) {
            removePicture()
        }
    }
    private var changeImageLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()) {
            result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val selectedImage = result.data?.data
            activity?.let {
                selectedImage?.let { uri ->
                    val tempFile = File.createTempFile("temporaryFile", "jpeg")
                    tempFile.deleteOnExit()
                    val out = FileOutputStream(tempFile)
                    it.contentResolver.openInputStream(uri)?.copyTo(out)
                    file = tempFile
                }

                if (selectedImage == null) {
                    file = requestedFile
                    requestedFile = null
                }
                pictureWasChanged.postValue(Event(file))
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_picture, container, false)

        view.add_picture_button.setOnClickListener { capturePhoto() }
        view.picture_view.setOnClickListener {
            file?.path?.let { path ->
                val intent = Intent(context, PictureActivity::class.java).apply {
                    putExtra(EXTRA_PICTURE_FILE, path)
                    putExtra(PictureActivity.EXTRA_SHOW_DELETE_BUTTON, true)
                }
                showImageLauncher.launch(intent)
                activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
            }
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        savedInstanceState?.let { state ->
            (state.getSerializable(STATE_PICTURE_FILE) as? File?)?.let {
                file = it
            }
            (state.getSerializable(STATE_REQUESTED_FILE) as? File?)?.let {
                requestedFile = it
            }
        }
    }

    override fun onDestroy() {
        if (activity?.isFinishing == true) {
            requestedFile?.delete()
            file?.delete()
        }

        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        file?.let {
            outState.putSerializable(STATE_PICTURE_FILE, it)
        }
        requestedFile?.let {
            outState.putSerializable(STATE_REQUESTED_FILE, it)
        }

        super.onSaveInstanceState(outState)
    }

    fun setCanTakePhoto(canTake: Boolean) {
        if (canTake) add_picture_button.show()
        else add_picture_button.hide()
    }

    /**
     * Starts the camera activity to take a photo.
     */
    private fun capturePhoto() {
        try {
            val pickIntent = Intent()
            pickIntent.type = "image/*"
            pickIntent.action = Intent.ACTION_GET_CONTENT
            val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            context?.let { ctx ->
                takePhotoIntent.also { intent ->
                    intent.resolveActivity(ctx.packageManager)?.also {
                        requestedFile = Utils.createTmpFile(ctx, PICTURE_FORMAT).also {
                            val photoURI: Uri =
                                    if (Build.VERSION.SDK_INT >= 24) {
                                        FileProvider.getUriForFile(
                                                ctx, "${BuildConfig.APPLICATION_ID}.fileprovider", it
                                        )
                                    } else {
                                        it.toUri()
                                    }
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        }
                    }
                }
            }
            val chooserIntent = Intent.createChooser(pickIntent, "").apply {
                putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(takePhotoIntent))
            }
            changeImageLauncher.launch(chooserIntent)
        } catch (e: IOException) {
            captureException(Exception("Unexpected error in capturePhoto", e))
        }
    }

    /**
     * Updates the currently shown image to [file], if it is not null.
     */
    private fun showFile() {
        file?.let { f ->
            Glide.with(this)
                    .load(f)
                    .fitCenter()
                    .centerCrop()
                    .error(defaultPicture)
                    .into(picture_view)
        }
    }

    private fun showPictureFromUri() {
        pictureUri?.let { f ->
            Glide.with(this)
                .load(f)
                .fitCenter()
                .centerCrop()
                .error(R.drawable.basket_default_picture)
                .into(picture_view)
        }
    }

    private fun removePicture() {
        file = null
        picture_view.setImageBitmap(
            BitmapFactory.decodeResource(resources, defaultPicture)
        )
        pictureWasChanged.postValue(Event(file))
    }
}
