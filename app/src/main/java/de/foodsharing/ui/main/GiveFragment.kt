package de.foodsharing.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.baskets.MyBasketsFragment
import de.foodsharing.ui.fsp.NearbyFoodSharePointsFragment
import de.foodsharing.ui.newbasket.NewBasketActivity
import kotlinx.android.synthetic.main.fragment_give.view.add_basket_button
import kotlinx.android.synthetic.main.fragment_give.view.fragment_give_pull_refresh

class GiveFragment : BaseFragment(), Injectable {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_give, container, false)

        view.add_basket_button.setOnClickListener {
            NewBasketActivity.start(requireContext())
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }

        if (savedInstanceState == null) {
            activity?.supportFragmentManager?.let {
                it.beginTransaction()
                    .replace(R.id.fragment_my_baskets, MyBasketsFragment(), MyBasketsFragment::class.qualifiedName)
                    .replace(
                        R.id.fragment_nearby_fsps,
                        NearbyFoodSharePointsFragment(),
                        NearbyFoodSharePointsFragment::class.qualifiedName
                    )
                    .commit()

                view.fragment_give_pull_refresh.setOnRefreshListener {
                    (it.findFragmentByTag(MyBasketsFragment::class.qualifiedName) as? MyBasketsFragment)
                        ?.reload()
                    (it.findFragmentByTag(NearbyFoodSharePointsFragment::class.qualifiedName) as?
                            NearbyFoodSharePointsFragment)?.reload()
                    view.fragment_give_pull_refresh.isRefreshing = false
                }
            }
        }

        return view
    }
}
