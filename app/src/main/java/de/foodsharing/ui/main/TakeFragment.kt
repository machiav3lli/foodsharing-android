package de.foodsharing.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.baskets.NearbyBasketsFragment
import de.foodsharing.ui.pickups.FuturePickupsFragment
import kotlinx.android.synthetic.main.fragment_take.view.fragment_take_pull_refresh

class TakeFragment : BaseFragment(), Injectable {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_take, container, false)

        if (savedInstanceState == null) {
            activity?.supportFragmentManager?.let {
                it.beginTransaction()
                    .replace(
                        R.id.fragment_future_pickups,
                        FuturePickupsFragment(),
                        FuturePickupsFragment::class.qualifiedName
                    )
                    .replace(
                        R.id.fragment_nearby_baskets,
                        NearbyBasketsFragment(),
                        NearbyBasketsFragment::class.qualifiedName
                    )
                    .commit()

                view.fragment_take_pull_refresh.setOnRefreshListener {
                    (it.findFragmentByTag(FuturePickupsFragment::class.qualifiedName) as? FuturePickupsFragment)
                        ?.reload()
                    (it.findFragmentByTag(NearbyBasketsFragment::class.qualifiedName) as? NearbyBasketsFragment)
                        ?.reload()
                    view.fragment_take_pull_refresh.isRefreshing = false
                }
            }
        }

        return view
    }
}
