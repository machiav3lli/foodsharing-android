package de.foodsharing.ui.posts

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import de.foodsharing.R
import de.foodsharing.model.PostPicture
import de.foodsharing.ui.picture.PictureActivity
import de.foodsharing.utils.inflate
import kotlinx.android.synthetic.main.item_post_picture.view.post_picture

class PostPictureListAdapter(val context: Context) :
        RecyclerView.Adapter<PostPictureListAdapter.PostPictureHolder>() {
    private var pictures = emptyList<PostPicture>()

    override fun getItemCount() = pictures.size

    override fun onBindViewHolder(holder: PostPictureHolder, position: Int) {
        if (position in 0 until itemCount) {
            holder.bind(pictures[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int) =
        PostPictureHolder(parent.inflate(R.layout.item_post_picture, false), context)

    fun setPictures(pictures: List<PostPicture>) {
        this.pictures = pictures
        notifyDataSetChanged()
    }

    class PostPictureHolder(val view: View, val context: Context) :
        RecyclerView.ViewHolder(view) {

        fun bind(picture: PostPicture) {
            view.post_picture.let {
                Glide.with(context)
                    .load(picture.mediumSizeUrl())
                    .into(it)
                it.setOnClickListener {
                    PictureActivity.start(context, picture.fullSizeUrl())
                }
            }
        }
    }
}
