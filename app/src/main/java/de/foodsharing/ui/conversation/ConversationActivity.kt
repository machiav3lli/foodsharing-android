package de.foodsharing.ui.conversation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.stfalcon.chatkit.messages.FsMessageListAdapter
import com.stfalcon.chatkit.messages.MessagesListAdapter
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Conversation
import de.foodsharing.model.User
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.base.ErrorStateRetrySnackbar
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.profile.ProfileActivity
import de.foodsharing.ui.users.UserListActivity
import de.foodsharing.utils.ChatkitGlideImageLoader
import kotlinx.android.synthetic.main.activity_conversation.*
import java.util.Date
import javax.inject.Inject

class ConversationActivity : BaseActivity(), Injectable {

    companion object {
        const val EXTRA_CONVERSATION_ID = "cid"
        private const val EXTRA_FOODSHARER_ID = "fsid"
        private const val EXTRA_SHARE_MESSAGE = "message"

        fun start(context: Context, userId: Int) {
            val extras = bundleOf(EXTRA_FOODSHARER_ID to userId)
            val intent = Intent(context, ConversationActivity::class.java).putExtras(extras)
            context.startActivity(intent)
        }

        fun start(context: Context, conversationId: Int, message: String?) {
            val extras = bundleOf(
                    EXTRA_CONVERSATION_ID to conversationId,
                    EXTRA_SHARE_MESSAGE to message
            )
            val intent = Intent(context, ConversationActivity::class.java).putExtras(extras)
            context.startActivity(intent)
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: ConversationViewModel by viewModels { viewModelFactory }

    private var messageAdapter: FsMessageListAdapter? = null
    private var messageAdapterDiff: AsyncListDiffer<MessagesListAdapter<ChatkitMessage>.Wrapper<*>>? = null

    private lateinit var menu: Menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rootLayoutID = R.id.container
        setContentView(R.layout.activity_conversation)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        message_input.visibility = GONE

        message_input.setInputListener { message ->
            val trimmedMessage = message.toString().trim()
            if (trimmedMessage.isNotEmpty()) {
                viewModel.sendMessage(trimmedMessage)
            }

            // Always return false since we clear the message manually
            false
        }

        if (intent.hasExtra(EXTRA_CONVERSATION_ID)) {
            viewModel.id = intent.getIntExtra(EXTRA_CONVERSATION_ID, -1)
        } else if (intent.hasExtra(EXTRA_FOODSHARER_ID)) {
            viewModel.foodsharerId = intent.getIntExtra(EXTRA_FOODSHARER_ID, -1)
        } else {
            showMessage(getString(R.string.conversation_404))
        }

        if (savedInstanceState == null) {
            intent.getStringExtra(EXTRA_SHARE_MESSAGE)?.let {
                message_input.inputEditText.setText(it)
            }
        }

        bindViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        this.menu = menu
        menuInflater.inflate(R.menu.conversation_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.action_copy -> {
            messageAdapter?.let {
                // Fix for: https://sentry.io/share/issue/3c88886a9efd4551803ad34e5d81fdde/
                if (it.selectedMessages.isEmpty()) return@let
                val copyMessage = resources.getQuantityString(
                    R.plurals.copied_messages,
                    it.selectedMessages.size,
                    it.selectedMessages.size
                )
                super.showMessage(copyMessage, duration = Snackbar.LENGTH_SHORT)
                it.copySelectedMessagesText(this, { m -> m.text }, true)
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun bindViewModel() {
        viewModel.currentUserId.observe(this) {
            initAdapter(senderId = it.toString())

            // If messages not shown yet but loaded, display them
            if (message_input.visibility != VISIBLE) {
                viewModel.conversation.value?.let { m -> display(m) }
            }
        }

        viewModel.updateInput.observe(this, EventObserver {
            message_input.inputEditText.setText(it)
        })

        viewModel.isSendingMessage.observe(this) {
            message_input.button.isEnabled = !it
        }

        viewModel.conversation.observe(this) {
            display(it)
        }

        viewModel.showError.observe(this, EventObserver {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })

        ErrorStateRetrySnackbar(lifecycle, viewModel.errorState, viewModel::tryAgain, this, container)
    }

    private fun initAdapter(senderId: String) {
        val adapter = FsMessageListAdapter(senderId,
            ChatkitGlideImageLoader(this)
        )
        adapter.setDateHeadersFormatter(MessageDateFormatter(this))
        adapter.enableSelectionMode {
            menu.findItem(R.id.action_copy).isVisible = it > 0
        }

        adapter.registerViewClickListener(R.id.messageUserAvatar) { _, message ->
            ProfileActivity.start(this, message.message.author.id)
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }

        adapter.setLoadMoreListener { _, _ -> viewModel.loadNext() }

        recycler_view.setAdapter(adapter)

        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)

                // If a new message was added, scroll to it
                if (positionStart == 0) {
                    recycler_view.layoutManager?.scrollToPosition(0)
                }
            }
        })

        messageAdapterDiff = AsyncListDiffer(adapter,
            object : DiffUtil.ItemCallback<MessagesListAdapter<ChatkitMessage>.Wrapper<*>>() {

            override fun areItemsTheSame(
                oldItem: MessagesListAdapter<ChatkitMessage>.Wrapper<*>,
                newItem: MessagesListAdapter<ChatkitMessage>.Wrapper<*>
            ): Boolean {
                if (oldItem.item is ChatkitMessage && newItem.item is ChatkitMessage) {
                    return (oldItem.item as ChatkitMessage).id == (newItem.item as ChatkitMessage).id
                }
                return oldItem.item == newItem.item
            }

            override fun areContentsTheSame(
                oldItem: MessagesListAdapter<ChatkitMessage>.Wrapper<*>,
                newItem: MessagesListAdapter<ChatkitMessage>.Wrapper<*>
            ): Boolean {
                if (oldItem.item is ChatkitMessage && newItem.item is ChatkitMessage) {
                    return (oldItem.item as ChatkitMessage) == (newItem.item as ChatkitMessage)
                }
                if (oldItem.item is Date && newItem.item is Date) {
                    return (oldItem.item as Date) == (newItem.item as Date)
                }
                return false
            }
        })

        messageAdapter = adapter
    }

    private fun display(conversation: Conversation) {
        messageAdapter?.let { adapter ->
            message_input.visibility = VISIBLE

            supportActionBar?.title = getTitle(conversation)
            getSubtitle(conversation)?.let {
                supportActionBar?.subtitle = it
            }

            val sortedItems = conversation.messages.map {
                ChatkitMessage(it)
            }
            val sortedItemsWithHeaders = adapter.wrapItemsBasedOnCurrent(sortedItems)
            messageAdapterDiff?.submitList(sortedItemsWithHeaders) {
                adapter.setItemsWithoutNotify(sortedItemsWithHeaders)
            }

            progress_bar.visibility = GONE
            message_input.visibility = VISIBLE

            toolbar.setOnClickListener {
                val others = conversation.members.filter { it.id != viewModel.currentUserId.value }
                if (others.isEmpty()) showMessage(
                    getString(R.string.profile_404),
                    duration = Snackbar.LENGTH_SHORT
                )
                else openProfiles(others, conversation.title ?: getString(R.string.user_list_title))
            }
        }
    }

    private fun openProfiles(users: List<User>, title: String) {
        if (users.size == 1)
            ProfileActivity.start(this, users[0])
        else
            UserListActivity.start(this, viewModel.id!!, title)
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }

    /**
     * Formats the names of chat members for the title.
     */
    private fun formatUserNames(users: List<User>): String {
        return users.filter { it.id != viewModel.currentUserId.value }
            .mapNotNull { it.name }
            .joinToString(", ")
    }

    private fun getTitle(conversation: Conversation): String =
        if (conversation.members.size > 2) {
            conversation.title ?: formatUserNames(conversation.members)
        } else {
            formatUserNames(conversation.members)
        }

    private fun getSubtitle(conversation: Conversation): String? =
        if (conversation.members.size > 2) {
            if (conversation.members.size > 5) {
                getString(R.string.conversation_members_subtitle, conversation.members.size)
            } else {
                formatUserNames(conversation.members)
            }
        } else {
            null
        }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }
}
