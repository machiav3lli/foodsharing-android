package de.foodsharing.ui.pickup

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Store
import de.foodsharing.model.StorePickup
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.pickups.FuturePickupDateFormatter
import de.foodsharing.utils.LINK_BASE_URL
import kotlinx.android.synthetic.main.activity_pickup.*
import java.util.Date
import javax.inject.Inject

class PickupActivity : BaseActivity(), Injectable {
    companion object {
        private const val STORE_URL = "$LINK_BASE_URL/?page=fsbetrieb&id=%d"

        private const val EXTRA_STORE = "store"
        private const val EXTRA_PICKUP_DATE = "date"

        fun start(context: Context, store: Store, date: Date) {
            val extras = bundleOf(EXTRA_STORE to store, EXTRA_PICKUP_DATE to date)
            val intent = Intent(context, PickupActivity::class.java).putExtras(extras)
            context.startActivity(intent)
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: PickupViewModel by viewModels { viewModelFactory }
    private var menu: Menu? = null
    private var dateFormatter = FuturePickupDateFormatter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rootLayoutID = R.id.pickup_content
        setContentView(R.layout.activity_pickup)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        bindViewModel()

        viewModel.store = intent.getSerializableExtra(EXTRA_STORE) as Store
        viewModel.date = intent.getSerializableExtra(EXTRA_PICKUP_DATE) as Date
        supportActionBar?.title = getString(R.string.pickup_title)
    }

    private fun bindViewModel() {
        viewModel.isLoading().observe(this) {
            progress_bar.visibility = if (it) View.VISIBLE else View.INVISIBLE
        }

        viewModel.error().observe(this, EventObserver {
            showErrorMessage(getString(it))
        })

        viewModel.pickup().observe(this) {
            it?.let {
                display(it)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.pickup_menu, menu)
        this.menu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.pickup_open_website_button -> {
            getStoreUrl()?.let {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(it))
                startActivity(browserIntent)
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun getStoreUrl(): String? {
        return viewModel.store?.id?.let { STORE_URL.format(it) }
    }

    private fun showErrorMessage(error: String) {
        progress_bar.visibility = View.GONE
        showMessage(error, duration = Snackbar.LENGTH_LONG)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    private fun display(pickup: StorePickup) {
        pickup_content_view.visibility = View.VISIBLE
        progress_bar.visibility = View.GONE
        store_name.text = viewModel.store?.name
        pickup_date.text = viewModel.date?.let { dateFormatter.format(it).toUpperCase() }

        val transaction = supportFragmentManager.beginTransaction()
        pickup.occupiedSlots.forEach {
            transaction.add(R.id.pickup_users_layout, PickupSlotFragment().apply {
                arguments = bundleOf(PickupSlotFragment.EXTRA_SLOT to it)
            })
        }
        transaction.commit()
    }
}
