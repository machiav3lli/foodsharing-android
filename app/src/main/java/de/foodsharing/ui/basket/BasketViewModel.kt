package de.foodsharing.ui.basket

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.model.Basket
import de.foodsharing.services.AuthService
import de.foodsharing.services.BasketService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import de.foodsharing.utils.UserLocation
import io.reactivex.rxkotlin.Observables
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class BasketViewModel @Inject constructor(
    private val basketService: BasketService,
    auth: AuthService,
    private val userLocation: UserLocation
) : BaseViewModel() {
    var basketId: Int? = null
        set(value) {
            if (field != value) {
                field = value
                if (basket.value?.id != field) fetch()
            }
        }

    val isCurrentUser = MutableLiveData<Boolean>().apply { value = false }
    val isLoading = MutableLiveData<Boolean>()
    val showError = MutableLiveData<Event<Int>>()
    val showInfo = MutableLiveData<Event<Int>>()
    val basket = MutableLiveData<Basket>()
    val basketRemoved = MutableLiveData<Event<Unit>>()
    val distance = MutableLiveData<Double>()

    private var currentUserId: Int? = null

    init {
        request(auth.currentUser(), {
            currentUserId = it.id
            isCurrentUser.value = currentUserId != null && basket.value?.creator?.id == currentUserId
        }, {
            handleError(it)
        })
    }

    private fun fetch() {
        if (basketId != null) {
            isLoading.value = true

            request(Observables.combineLatest(basketService.get(basketId!!), userLocation.getUsersLocation()), {
                    (basketResponse, location) ->
                isLoading.value = false
                basket.value = basketResponse.basket
                distance.value = basketResponse.basket?.toCoordinate()?.distanceTo(location)

                isCurrentUser.value =
                    currentUserId != null && basket.value?.creator?.id == currentUserId
            }, {
                isLoading.value = false
                handleError(it)
            })
        } else {
            basket.value = null
        }
    }

    fun removeBasket() {
        basket.value?.let {
            isLoading.value = true

            request(basketService.remove(it.id), {
                isLoading.value = false
                basketRemoved.value = Event(Unit)
            }, { err ->
                isLoading.value = false
                handleError(err)
            })
        }
    }

    private fun handleError(error: Throwable) {
        val stringRes = if (error is HttpException && error.code() == 404) {
            R.string.basket_404
        } else if (error is IOException) {
            R.string.error_no_connection
        } else {
            error.printStackTrace()
            R.string.error_unknown
        }
        showError.value = Event(stringRes)
    }

    private fun handleRequestError(error: Throwable) {
        val stringRes = if (error is HttpException && error.code() == 400) {
            R.string.basket_request_empty_message_error
        } else if (error is HttpException && error.code() == 403) {
            R.string.basket_request_denied_error
        } else {
            handleError(error)
            return
        }
        showError.value = Event(stringRes)
    }

    fun withdrawRequest() {
        isLoading.value = true
        request(basketService.withdrawRequest(basketId!!).doOnTerminate {
            isLoading.postValue(false)
        }, {
            showInfo.value = Event(R.string.basket_withdrawed_request)
        }, {
            handleError(it)
        })
    }

    fun requestBasket(message: String) {
        isLoading.value = true
        request(basketService.request(basketId!!, message).doOnTerminate {
            isLoading.postValue(false)
        }, {
            showInfo.value = Event(R.string.basket_sent_request)
        }, {
            handleRequestError(it)
        })
    }
}
