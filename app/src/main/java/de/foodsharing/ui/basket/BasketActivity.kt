package de.foodsharing.ui.basket

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.services.PreferenceManager
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.conversation.ConversationActivity
import de.foodsharing.ui.editbasket.EditBasketActivity
import de.foodsharing.ui.picture.PictureActivity
import de.foodsharing.ui.profile.ProfileActivity
import de.foodsharing.utils.BASKET_MAP_ZOOM
import de.foodsharing.utils.LINK_BASE_URL
import de.foodsharing.utils.OsmdroidUtils
import de.foodsharing.utils.Utils
import de.foodsharing.utils.Utils.getBasketMarkerIconBitmap
import de.foodsharing.utils.getDisplayName
import de.foodsharing.utils.setGone
import de.foodsharing.utils.setVisible
import kotlinx.android.synthetic.main.activity_basket.avatar
import kotlinx.android.synthetic.main.activity_basket.basket_content_view
import kotlinx.android.synthetic.main.activity_basket.basket_created_at
import kotlinx.android.synthetic.main.activity_basket.basket_creator_name
import kotlinx.android.synthetic.main.activity_basket.basket_description
import kotlinx.android.synthetic.main.activity_basket.basket_location_view
import kotlinx.android.synthetic.main.activity_basket.basket_message_button
import kotlinx.android.synthetic.main.activity_basket.basket_mobile_info
import kotlinx.android.synthetic.main.activity_basket.basket_mobile_label
import kotlinx.android.synthetic.main.activity_basket.basket_phone
import kotlinx.android.synthetic.main.activity_basket.basket_phone_info
import kotlinx.android.synthetic.main.activity_basket.basket_phone_label
import kotlinx.android.synthetic.main.activity_basket.basket_picture
import kotlinx.android.synthetic.main.activity_basket.basket_request_button
import kotlinx.android.synthetic.main.activity_basket.basket_request_count_label
import kotlinx.android.synthetic.main.activity_basket.basket_valid_until
import kotlinx.android.synthetic.main.activity_basket.basket_withdraw_request_button
import kotlinx.android.synthetic.main.activity_basket.distance_to_basket
import kotlinx.android.synthetic.main.activity_basket.progress_bar
import kotlinx.android.synthetic.main.activity_basket.toolbar
import kotlinx.android.synthetic.main.dialog_request_basket.basket_request_message_input
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import javax.inject.Inject

class BasketActivity : BaseActivity(), Injectable {

    companion object {
        private const val BASKET_URL = "$LINK_BASE_URL/essenskoerbe/%d"

        private const val EXTRA_BASKET_ID = "id"

        fun start(context: Context, basketId: Int) {
            val extras = bundleOf(EXTRA_BASKET_ID to basketId)
            val intent = Intent(context, BasketActivity::class.java).putExtras(extras)
            context.startActivity(intent)
        }
    }

    private val dateFormat = SimpleDateFormat("EEE, d MMM yyyy\nHH:mm", Locale.getDefault())
    private val dateFormatShort = SimpleDateFormat("EEE, d MMM\nHH:mm", Locale.getDefault())

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var preferenceManager: PreferenceManager

    private val viewModel: BasketViewModel by viewModels { viewModelFactory }
    private var mapSnapshotDetachAction: (() -> Unit)? = null
    private var menu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rootLayoutID = R.id.basket_content
        setContentView(R.layout.activity_basket)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        bindViewModel()

        if (intent.hasExtra(EXTRA_BASKET_ID)) {
            val id = intent.getIntExtra(EXTRA_BASKET_ID, -1)
            supportActionBar?.title = "${getString(R.string.basket_label_short)} #$id"
            viewModel.basketId = id
        } else if (intent.data?.pathSegments?.firstOrNull() == "essenskoerbe") {
            val id = intent.data!!.pathSegments.last().toIntOrNull()
            if (id != null) {
                supportActionBar?.title = "${getString(R.string.basket_label_short)} #$id"
                viewModel.basketId = id
            } else {
                showBasket404()
            }
        } else {
            showBasket404()
        }
    }

    private fun bindViewModel() {
        viewModel.isLoading.observe(this) {
            progress_bar.visibility = if (it) VISIBLE else INVISIBLE
        }

        viewModel.showInfo.observe(this, EventObserver {
            Toast.makeText(this, getString(it), Toast.LENGTH_LONG).show()
        })

        viewModel.showError.observe(this, EventObserver {
            showErrorMessage(getString(it))
        })

        viewModel.basket.observe(this) {
            display(it)
        }

        viewModel.basketRemoved.observe(this, EventObserver {
            finish()
        })

        viewModel.isCurrentUser.observe(this) {
            updateCurrentUser(it)
        }

        viewModel.distance.observe(this) {
            distance_to_basket.text = Utils.formatDistance(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.basket_menu, menu)
        this.menu = menu
        updateCurrentUser(viewModel.isCurrentUser.value == true)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.basket_copy_button -> {
            getBasketUrl()?.let {
                Utils.copyToClipboard(this, it)
                Toast.makeText(this, getString(R.string.copied_url), Toast.LENGTH_SHORT).show()
            }
            true
        }

        R.id.basket_share_button -> {
            getBasketUrl()?.let {
                Utils.openShareDialog(this, it)
            }
            true
        }
        R.id.basket_remove_button -> {
            viewModel.basket.value?.let {
                Utils.showQuestionDialog(this, getString(R.string.basket_remove_question)) { result ->
                    if (result) {
                        progress_bar.visibility = VISIBLE
                        viewModel.removeBasket()
                    }
                }
            }
            true
        }
        R.id.basket_edit_button -> {
            viewModel.basket.value?.let {
                EditBasketActivity.start(this, it)
            }
            true
        }
        R.id.basket_open_website_button -> {
            getBasketUrl()?.let {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(it))
                startActivity(browserIntent)
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun getBasketUrl(): String? {
        return viewModel.basket.value?.let { BASKET_URL.format(it.id) }
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onDestroy() {
        mapSnapshotDetachAction?.invoke()
        super.onDestroy()
    }

    private fun display(basket: Basket) {
        basket_created_at.text = formatDate(basket.createdAt)
        basket_valid_until.text = formatDate(basket.until)
        basket_description.text = basket.description
        basket_creator_name.text = basket.creator.getDisplayName(this)

        invalidateOptionsMenu()

        basket_content_view.visibility = VISIBLE
        progress_bar.visibility = GONE

        // load basket picture
        if (basket.picture.isNullOrEmpty()) basket_picture.visibility = GONE
        else basket.picture?.let {
            val photoType =
                if (preferenceManager.useLowResolutionImages) Utils.BasketPhotoType.MEDIUM
                else Utils.BasketPhotoType.NORMAL
            val pictureUrl = Utils.getBasketPhotoURL(it, photoType)
            Glide.with(this)
                .load(pictureUrl)
                .centerCrop()
                .error(R.drawable.basket_default_picture)
                .into(basket_picture)

            basket_picture.setOnClickListener {
                PictureActivity.start(this, pictureUrl)
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
            }
            basket_picture.visibility = VISIBLE
        }
        // load user picture

        Glide.with(this)
            .load(Utils.getUserPhotoURL(basket.creator, Utils.PhotoType.Q_130, 130))
            .placeholder(R.drawable.default_user_picture)
            .error(R.drawable.default_user_picture)
            .transform(CircleCrop())
            .into(avatar)

        val listener = View.OnClickListener {
            ProfileActivity.start(this, basket.creator)
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }
        avatar.setOnClickListener(listener)
        basket_creator_name.setOnClickListener(listener)

        updateCurrentUser(viewModel.isCurrentUser.value == true)

        val basketTitle = getString(R.string.basket_title, basket.creator.getDisplayName(this))

        mapSnapshotDetachAction = OsmdroidUtils.loadMapTileToImageView(
            basket_location_view, basket.toCoordinate(), BASKET_MAP_ZOOM,
            getBasketMarkerIconBitmap(this.applicationContext), preferenceManager.allowHighResolutionMap
        )
        basket_location_view.setOnClickListener {
            Utils.openCoordinate(this, basket.toCoordinate(), basketTitle)
        }
    }

    private fun showErrorMessage(error: String) {
        progress_bar.visibility = GONE
        showMessage(error, duration = Snackbar.LENGTH_LONG)
    }

    private fun showBasket404() {
        showErrorMessage(getString(R.string.basket_404))
    }

    /**
     * Updates the visibility of the option menu items and contact labels depending on whether the
     * current user is the owner of the basket.
     */
    private fun updateCurrentUser(isCurrentUser: Boolean) {
        val basket = viewModel.basket.value
        menu?.findItem(R.id.basket_remove_button)?.isVisible = isCurrentUser && basket != null
        menu?.findItem(R.id.basket_edit_button)?.isVisible = isCurrentUser && basket != null

        if (!isCurrentUser && basket != null && basket.contactTypes.isNotEmpty()) {
            if (basket.contactTypes.contains(Basket.CONTACT_TYPE_MESSAGE)) {
                basket_request_count_label.visibility = VISIBLE
                basket_request_count_label.text = resources.getQuantityString(R.plurals.basket_request_number,
                    basket.requestCount, basket.requestCount)

                val currentUserHasRequested = basket.requests.isNotEmpty()
                if (currentUserHasRequested) {
                    setVisible(basket_message_button, basket_withdraw_request_button)
                    basket_request_button.setGone()

                    basket_message_button.setOnClickListener {
                        ConversationActivity.start(this, basket.creator.id)
                    }

                    basket_withdraw_request_button.setOnClickListener {
                        Utils.showQuestionDialog(this, getString(R.string.basket_withdraw_question)) { result ->
                            if (result) {
                                viewModel.withdrawRequest()
                            }
                        }
                    }
                } else {
                    basket_request_button.setVisible()
                    setGone(basket_message_button, basket_withdraw_request_button)

                    basket_request_button.setOnClickListener {
                        lateinit var dialog: AlertDialog
                        val builder = AlertDialog.Builder(this)
                        builder.setTitle(R.string.request_button)
                        builder.setView(R.layout.dialog_request_basket)
                        builder.setPositiveButton(R.string.basket_request_dialog_label) { _, _ ->
                            viewModel.requestBasket(dialog.basket_request_message_input.text.toString())
                        }
                        builder.setNegativeButton(android.R.string.cancel, null)
                        dialog = builder.show()
                    }
                }
            } else {
                setGone(basket_message_button, basket_request_count_label,
                    basket_request_button, basket_withdraw_request_button)
            }

            if (basket.contactTypes.contains(Basket.CONTACT_TYPE_PHONE)) {
                basket_phone.setVisible()
                with(basket) {
                    if (phone.isNotEmpty()) {
                        basket_phone_info.text = phone
                        setVisible(basket_phone_label, basket_phone_info)
                    }
                    if (mobile.isNotEmpty()) {
                        basket_mobile_info.text = mobile
                        setVisible(basket_mobile_label, basket_mobile_info)
                    }
                }
            } else {
                basket_phone.setGone()
            }
        } else {
            setGone(basket_message_button, basket_request_count_label,
                basket_request_button, basket_withdraw_request_button)
        }
    }

    private fun formatDate(date: Date): String {
        val now = Calendar.getInstance()
        now.time = Date()

        val cal: Calendar = Calendar.getInstance()
        cal.time = date

        val format = if (cal[Calendar.YEAR] == now[Calendar.YEAR]) dateFormatShort else dateFormat
        return format.format(date)
    }
}
