package de.foodsharing.ui.community

import android.util.Log
import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.api.RegionAPI
import de.foodsharing.model.Community
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import de.foodsharing.utils.LOG_TAG
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class CommunityViewModel @Inject constructor(
    private val regionAPI: RegionAPI
) : BaseViewModel() {
    var regionId: Int? = null
        set(value) {
            if (field != value) {
                field = value
                fetch()
            }
        }
    val isLoading = MutableLiveData<Boolean>()
    val showError = MutableLiveData<Event<Int>>()
    val community = MutableLiveData<Community?>()

    private fun fetch() {
        regionId?.let { id ->
            isLoading.value = true

            request(regionAPI.communityDescription(id).doOnNext {
                isLoading.postValue(true)
            }, { c ->
                isLoading.value = false
                community.value = c
            }, { throwable ->
                isLoading.value = false
                handleError(throwable)
            })
        } ?: run {
            community.value = null
        }
    }

    private fun handleError(error: Throwable) {
        Log.e(LOG_TAG, "", error)
        val stringRes = if (error is HttpException && error.code() == 404) {
            R.string.region_404
        } else if (error is IOException) {
            R.string.error_no_connection
        } else {
            R.string.error_unknown
        }
        showError.value = Event(stringRes)
    }
}
