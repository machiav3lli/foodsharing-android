package de.foodsharing.ui.community

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Community
import de.foodsharing.ui.base.AuthRequiredBaseActivity
import de.foodsharing.ui.base.EventObserver
import io.noties.markwon.Markwon
import kotlinx.android.synthetic.main.activity_community.community_content_view
import kotlinx.android.synthetic.main.activity_community.community_description
import kotlinx.android.synthetic.main.activity_community.progress_bar
import kotlinx.android.synthetic.main.activity_community.toolbar
import javax.inject.Inject

class CommunityActivity : AuthRequiredBaseActivity(), Injectable {
    companion object {
        private const val EXTRA_REGION_ID = "id"

        fun start(context: Context, regionId: Int) {
            val extras = bundleOf(EXTRA_REGION_ID to regionId)
            val intent = Intent(context, CommunityActivity::class.java).putExtras(extras)
            context.startActivity(intent)
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val communityViewModel: CommunityViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rootLayoutID = R.id.community_content
        setContentView(R.layout.activity_community)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        bindViewModel()

        val id = intent.getIntExtra(EXTRA_REGION_ID, -1)
        supportActionBar?.title = "${getString(R.string.activity_community_title)}: #$id"
        communityViewModel.regionId = id
    }

    private fun bindViewModel() {
        communityViewModel.isLoading.observe(this) {
            progress_bar.visibility = if (it) View.VISIBLE else View.GONE
        }

        communityViewModel.showError.observe(this, EventObserver {
            showErrorMessage(getString(it))
        })

        communityViewModel.community.observe(this) {
            display(it)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    private fun display(community: Community?) {
        community?.let {
            supportActionBar?.title = it.name
        }

        val markwon = Markwon.create(this)
        markwon.setMarkdown(community_description, community?.description ?: "")

        community_content_view.visibility = View.VISIBLE
        progress_bar.visibility = View.GONE
    }

    private fun showErrorMessage(error: String) {
        progress_bar.visibility = View.GONE
        showMessage(error, duration = Snackbar.LENGTH_LONG)
    }
}
