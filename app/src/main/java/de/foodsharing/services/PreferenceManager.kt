package de.foodsharing.services

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import de.foodsharing.R
import de.foodsharing.model.Coordinate
import de.foodsharing.ui.base.LocationFilterComponent
import de.foodsharing.utils.DEFAULT_MAP_ZOOM
import org.apache.commons.lang3.LocaleUtils
import java.util.Locale
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.math.abs

@Singleton
class PreferenceManager @Inject constructor(
    private val preferences: SharedPreferences,
    private val context: Context
) {
    private val listeners: MutableMap<String, () -> Unit> = mutableMapOf()

    // Strong reference to the listener
    private val listener = SharedPreferences.OnSharedPreferenceChangeListener { _: SharedPreferences, key: String ->
        listeners[key]?.invoke()
    }

    init {
        preferences.registerOnSharedPreferenceChangeListener(listener)
    }

    var isLoggedIn: Boolean
        get() = preferences.getBoolean(context.getString(R.string.preferenceLoggedIn), false)
        set(value) = preferences.edit { putBoolean(context.getString(R.string.preferenceLoggedIn), value) }

    var wasLastContactByMessage: Boolean
        get() = preferences.getBoolean(context.getString(R.string.preferenceLastContactByMessage), false)
        set(value) = preferences.edit { putBoolean(context.getString(R.string.preferenceLastContactByMessage), value) }

    var wasLastContactByPhone: Boolean
        get() = preferences.getBoolean(context.getString(R.string.preferenceLastContactByMessage), false)
        set(value) = preferences.edit { putBoolean(context.getString(R.string.preferenceLastContactByPhone), value) }

    var mapZoom: Double
        get() = preferences.getFloat(context.getString(R.string.preferenceMapZoom), DEFAULT_MAP_ZOOM.toFloat())
            .toDouble()
        set(value) = preferences.edit { putFloat(context.getString(R.string.preferenceMapZoom), value.toFloat()) }

    var mapCenterCoordinate: Coordinate?
        get() {
            if (!preferences.contains(context.getString(R.string.preferenceMapCenterLat))) return null
            if (!preferences.contains(context.getString(R.string.preferenceMapCenterLon))) return null
            val lat = preferences.getFloat(context.getString(R.string.preferenceMapCenterLat), 0.0f).toDouble()
            val lon = preferences.getFloat(context.getString(R.string.preferenceMapCenterLon), 0.0f).toDouble()
            // Due to a previous bug the map was sometimes centered initially at 0,0. When the user changed the camera
            // position this value was stored in the preferences. Because of that we discard all values close to
            // this coordinate.
            if (abs(lat) < 4.0 && abs(lon) < 4.0) {
                return null
            }
            return Coordinate(lat, lon)
        }
        set(value) {
            preferences.edit {
                if (value == null) {
                    remove(context.getString(R.string.preferenceMapCenterLat))
                            .remove(context.getString(R.string.preferenceMapCenterLon))
                } else {
                    putFloat(context.getString(R.string.preferenceMapCenterLat), value.lat.toFloat())
                            .putFloat(context.getString(R.string.preferenceMapCenterLon), value.lon.toFloat())
                }
            }
        }

    var useLowResolutionImages: Boolean
        get() = preferences.getBoolean(context.getString(R.string.preferenceImagesLowRes), false)
        set(value) = preferences.edit { putBoolean(context.getString(R.string.preferenceImagesLowRes), value) }

    var allowHighResolutionMap: Boolean
        get() = preferences.getBoolean(context.getString(R.string.preferenceAllowHighResMap), true)
        set(value) = preferences.edit { putBoolean(context.getString(R.string.preferenceAllowHighResMap), value) }

    var pushNotificationsEnabled: Boolean?
        get() = if (preferences.contains(context.getString(R.string.preferenceEnablePushNotifications))) {
                preferences.getBoolean(context.getString(R.string.preferenceEnablePushNotifications), false)
            } else {
                null
            }
        set(value) = preferences.edit {
            if (value != null) {
                putBoolean(context.getString(R.string.preferenceEnablePushNotifications), value)
            } else {
                remove(context.getString(R.string.preferenceEnablePushNotifications))
            }
        }

    fun observePushNotificationEnabled(listener: (Boolean?) -> Unit) {
        listeners[context.getString(R.string.preferenceEnablePushNotifications)] = {
            listener(pushNotificationsEnabled)
        }
    }

    var pushPublicKey: String?
        get() = preferences.getString(context.getString(R.string.preferencePushPublicKey), null)
        set(value) = preferences.edit { putString(context.getString(R.string.preferencePushPublicKey), value) }

    var pushToken: String?
        get() = preferences.getString(context.getString(R.string.preferencePushToken), null)
        set(value) = preferences.edit { putString(context.getString(R.string.preferencePushToken), value) }

    var pushSubscription: String?
        get() = preferences.getString(context.getString(R.string.preferencePushSubscription), null)
        set(value) = preferences.edit { putString(context.getString(R.string.preferencePushSubscription), value) }

    var pushSubscriptionId: Int?
        get() {
            val value = preferences.getInt(context.getString(R.string.preferencePushSubscriptionId), -1)
            return if (value >= 0) value else null
        }
        set(value) {
            preferences.edit {
                if (value == null)
                    remove(context.getString(R.string.preferencePushSubscriptionId))
                else
                    putInt(context.getString(R.string.preferencePushSubscriptionId), value)
            }
        }

    fun observePushTokenOrPublicKeyUpdated(listener: () -> Unit) {
        listeners[context.getString(R.string.preferencePushPublicKey)] = {
            listener()
        }
        listeners[context.getString(R.string.preferencePushToken)] = {
            listener()
        }
    }

    /**
     * This is null if the user was not yet asked if logging to Sentry is allowed.
     */
    var isSentryEnabled: Boolean?
        get() {
            val key = context.getString(R.string.preferenceSentryEnabled)
            return if (preferences.contains(key)) preferences.getBoolean(key, false) else null
        }
        set(value) {
            preferences.edit {
                if (value == null)
                    remove(context.getString(R.string.preferenceSentryEnabled))
                else
                    putBoolean(context.getString(R.string.preferenceSentryEnabled), value)
            }
        }

    var nearbyFSPsLocationType: LocationFilterComponent.LocationType
        get() {
            val index = preferences.getInt(context.getString(R.string.preferenceNearbyFSPsLocationType), 0)
            return LocationFilterComponent.LocationType.values()[index]
        }
        set(value) = preferences.edit {
            putInt(context.getString(R.string.preferenceNearbyFSPsLocationType), value.ordinal)
        }

    var nearbyFSPsDistance: Int
        get() = preferences.getInt(context.getString(R.string.preferenceNearbyFSPsDistance), 0)
        set(value) = preferences.edit { putInt(context.getString(R.string.preferenceNearbyFSPsDistance), value) }

    var nearbyBasketsLocationType: LocationFilterComponent.LocationType
        get() {
            val index = preferences.getInt(context.getString(R.string.preferenceNearbyBasketsLocationType), 0)
            return LocationFilterComponent.LocationType.values()[index]
        }
        set(value) = preferences.edit {
            putInt(context.getString(R.string.preferenceNearbyBasketsLocationType), value.ordinal)
        }

    var nearbyBasketsDistance: Int
        get() = preferences.getInt(context.getString(R.string.preferenceNearbyBasketsDistance), 0)
        set(value) = preferences.edit { putInt(context.getString(R.string.preferenceNearbyBasketsDistance), value) }

    var language: Locale
        get() = LocaleUtils.toLocale(preferences.getString(context.getString(R.string.preferenceLanguage), null))
            ?: Locale.GERMAN
        set(value) = preferences.edit { putString(context.getString(R.string.preferenceLanguage), value.toString()) }
}
