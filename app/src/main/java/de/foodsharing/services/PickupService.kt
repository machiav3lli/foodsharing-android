package de.foodsharing.services

import de.foodsharing.api.PickupAPI
import de.foodsharing.model.StorePickup
import de.foodsharing.model.UserPickup
import io.reactivex.Observable
import io.reactivex.Single
import java.util.Date
import javax.inject.Inject

class PickupService @Inject constructor(
    private val pickupAPI: PickupAPI
) {
    fun getRegisteredPickups(): Observable<List<UserPickup>> {
        return pickupAPI.getRegisteredPickups()
    }

    fun getPickup(storeId: Int, date: Date): Single<StorePickup> {
        return pickupAPI.getPickupsForStore(storeId).firstOrError().map {
            it.pickups?.first { pickup -> pickup.date == date }
        }
    }
}
