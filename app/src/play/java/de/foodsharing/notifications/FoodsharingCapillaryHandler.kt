package de.foodsharing.notifications

import android.content.Context
import com.google.capillary.NoSuchKeyException
import com.google.capillary.android.CapillaryHandler
import com.google.capillary.android.CapillaryHandlerErrorCode
import com.google.capillary.internal.CapillaryPublicKey
import com.google.capillary.internal.WrappedWebPushPublicKey
import com.google.crypto.tink.subtle.Base64
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.google.gson.JsonObject
import de.foodsharing.model.PublicKey
import de.foodsharing.services.PreferenceManager
import java.nio.charset.Charset
import javax.inject.Inject

/**
 * Handler which is notified if the encryption key are updated and when a new message is decrypted.
 */
class FoodsharingCapillaryHandler @Inject constructor(
    val context: Context,
    val gson: Gson,
    val notificationFactory: NotificationFactory,
    val preferences: PreferenceManager
) : CapillaryHandler {
    private val TAG = FoodsharingCapillaryHandler::class.java.simpleName

    init {
        CapillaryUtils.initialize(context)
        if (preferences.pushPublicKey == null) {
            FirebaseInstanceId.getInstance().isFcmAutoInitEnabled = true
            val keyManager = CapillaryUtils.getKeyManager(context)
            try {
                keyManager.getPublicKey(false, this, false)
            } catch (e: NoSuchKeyException) {
                keyManager.generateKeyPair(false)
                keyManager.getPublicKey(false, this, null)
            }
        }
    }

    /**
     * New public capillary key
     */
    override fun handlePublicKey(isAuthKey: Boolean, publicKey: ByteArray, extra: Any?) {
        val capillaryPublicKey = CapillaryPublicKey.parseFrom(publicKey)
        val webpushPublicKey = WrappedWebPushPublicKey.parseFrom(capillaryPublicKey.keyBytes)
        val publicKeyBytes = webpushPublicKey.keyBytes
        val authSecret = webpushPublicKey.authSecret

        val fullPublicKey = PublicKey(
            Base64.encode(publicKeyBytes.toByteArray()),
            Base64.encode(authSecret.toByteArray()),
            capillaryPublicKey.keychainUniqueId,
            capillaryPublicKey.serialNumber
        )

        // Update public key in the preferences
        preferences.pushPublicKey = gson.toJson(fullPublicKey)
    }

    /**
     * This Capillary public key was generated due to a failure in decrypting a Capillary ciphertext.
     * Therefore, this method first registers the new Capillary public key with the application
     * server, and then requests the Capillary ciphertext to be resend.
     */
    override fun handlePublicKey(isAuthKey: Boolean, publicKey: ByteArray, ciphertext: ByteArray?, extra: Any?) {
        // Drop the push message and resubscribe to the backend
        // Delete the firebase instance id to trigger a resubscribe
        handlePublicKey(isAuthKey, publicKey, extra)
    }

    /**
     * This indicates that a Capillary ciphertext was saved to be decrypted later.
     */
    override fun authCiphertextSavedForLater(ciphertext: ByteArray?, extra: Any?) {
        // Do nothing
    }

    /**
     * This indicates an error.
     */
    override fun error(errorCode: CapillaryHandlerErrorCode?, ciphertext: ByteArray?, extra: Any?) {
    }

    /**
     * Shows the decrypted message as an Android notification.
     */
    override fun handleData(isAuthKey: Boolean, data: ByteArray, extra: Any?) {
        try {
            val stringData = data.toString(Charset.forName("UTF-8"))
            val json = gson.fromJson(stringData, JsonObject::class.java)
            val messageClass = when (json.get("t").asString) {
                CONVERSATION_PUSH_MESSAGE_TYPE -> ConversationPushMessage::class.java
                DEFAULT_PUSH_MESSAGE_TYPE -> DefaultPushMessage::class.java
                // Ignore unknown message types
                else -> return
            }
            val notification = gson.fromJson(json, messageClass)
            notificationFactory.createNotification(notification)
        } catch (e: Exception) {
            // Failed to read the message
            e.printStackTrace()
        }
    }
}
