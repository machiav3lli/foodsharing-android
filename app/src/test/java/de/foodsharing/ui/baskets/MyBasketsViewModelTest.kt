package de.foodsharing.ui.baskets

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.whenever
import de.foodsharing.api.BasketAPI
import de.foodsharing.model.Basket
import de.foodsharing.services.BasketService
import de.foodsharing.services.ProfileService
import de.foodsharing.test.configureTestSchedulers
import de.foodsharing.test.createRandomBasket
import de.foodsharing.test.createRandomProfile
import de.foodsharing.test.createRandomUser
import de.foodsharing.utils.CurrentUserLocation
import de.foodsharing.utils.UserLocation
import io.reactivex.Observable
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.ArgumentMatchers.anyDouble
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class MyBasketsViewModelTest {
    @Mock
    lateinit var basketService: BasketService

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        configureTestSchedulers()
    }

    @Test
    fun `fetch baskets`() {
        val creator = createRandomUser()
        val baskets = mutableListOf<Basket>()
        val numBaskets = 10
        for (i in 1..numBaskets) {
            baskets.add(createRandomBasket(creator))
        }

        whenever(basketService.list()) doReturn Observable.just(BasketAPI.BasketListResponse().apply {
            this.baskets = baskets
        })

        val viewModel = MyBasketsViewModel(basketService)
        val myBaskets = viewModel.baskets.value!!

        Assert.assertEquals(numBaskets, myBaskets.size)
    }
}
