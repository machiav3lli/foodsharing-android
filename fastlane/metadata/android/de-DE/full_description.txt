Endlich ist sie da! Die Test-Version der Foodsharing App. Aktuell unterstützt die App folgende Funktionen:

- Nachrichten
- Erstellen und Verwalten von Essenskörben
- Kartenfunktion mit Fair-Teilern und Essenskörben

Wir hoffen ihr findet mit der App ein paar nützliche Funktionen, die euch den Umgang mit foodsharing.network erleichtern. In Zukunft werden wir die App ständig erweitern, um bestehende Funktionen zu verbessern und neue hinzuzufügen. Falls ihr Verbesserungsvorschläge oder Wünsche habt, schreibt uns gerne unter <a href='mailto:it@foodsharing.network'>it@foodsharing.network</a>.

Falls ihr oder jemand den ihr kennt, Interesse hat uns bei der Entwicklung der App oder Webseite zu unterstützen meldet euch unter <a href='mailto:it@foodsharing.network'>it@foodsharing.network</a> oder schaut unter <a href='https://devdocs.foodsharing.network/it-tasks.html'>devdocs.foodsharing.network/it-tasks.html</a> vorbei.

Doch was ist foodsharing überhaupt?

Seit 2012 retten wir täglich tonnenweise gute Lebensmittel vor dem Müll. Wir verteilen sie ehrenamtlich und kostenfrei im Bekanntenkreis, der Nachbarschaft, in Obdachlosenheimen, Schulen, Kindergärten und über die Plattform foodsharing.network. Unsere öffentlich zugänglichen Regale und Kühlschränke, sogenannte „Fair-Teiler“, stehen allen zur Verfügung. 200.000 Menschen aus Deutschland, Österreich und der Schweiz nutzen regelmäßig die Internetplattform nach dem Motto: „Teile Lebensmittel, anstatt sie wegzuwerfen!“. Inzwischen engagieren sich darüber hinaus 56.000 Menschen ehrenamtlich als Foodsaver*innen, indem sie überproduzierte Lebensmittel von Bäckereien, Supermärkten, Kantinen und Großhändlern abholen und verteilen. Das geschieht kontinuierlich über 500 Mal am Tag bei fast 5.500 Kooperationspartnern.
