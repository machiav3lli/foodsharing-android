# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## Unreleased

## [0.7.1] - 2022-10-18

### Fixed
- Basket lists are updated correctly on reload !278 [@alex.simm](https://gitlab.com/alex.simm)
- Added a missing injector for the PickLocationActivity !278 [@alex.simm](https://gitlab.com/alex.simm)

## [0.7.0] - 2022-10-13

### Fixed
- Fix for open browser link !277 [@chriswalg](https://gitlab.com/chriswalg)

## [0.6.9] - 2022-10-07

- Some layout fixes !276 [@alex.simm](https://gitlab.com/alex.simm)

### Fixed

- Enable loading profile pictures from the new upload API !222 [@alex.simm](https://gitlab.com/alex.simm)

## [0.6.8] - 2022-10-03

### Fixed

- Enable loading profile pictures from the new upload API !222 [@alex.simm](https://gitlab.com/alex.simm)
- Fixed parsing error of empty coordinate strings !239 [@alex.simm](https://gitlab.com/alex.simm)
- Fix the endless loop of exceptions before login after reinstalling the app !257 [@alex.simm](https://gitlab.com/alex.simm)
- Fixed the coordinates object in the profile returned from the API #137 !261 [@alex.simm](https://gitlab.com/alex.simm)

### Added

- Added a settings activity to the drawer menu !223 [@alex.simm](https://gitlab.com/alex.simm)
- Adds push notifications for new chat messages !214 !253 [@dthulke](https://gitlab.com/dthulke)
- Add a dialog which allows selecting the location type and radius for later use !238 [@alex.simm](https://gitlab.com/alex.simm)
- Added the list of future pickups in the "take" fragment !268 [@alex.simm](https://gitlab.com/alex.simm)
- It is possible to change the language in the settings !270 [@alex.simm](https://gitlab.com/alex.simm)
- Community markers are being shown on the map !263 [@alex.simm](https://gitlab.com/alex.simm)
- Activity that shows details about a future pickup !272 [@alex.simm](https://gitlab.com/alex.simm)

### Changed

- Use `ComponentActivity#viewModels` extension function. !249 [@tbsprs](https://gitlab.com/tbsprs)
- Use `String#toUri` and `File#toUri` extension functions. !249 [@tbsprs](https://gitlab.com/tbsprs)
- Use `ShareCompat.IntentBuilder`. !249 [@tbsprs](https://gitlab.com/tbsprs)
- Use `createBitmap` convenience function. !249 [@tbsprs](https://gitlab.com/tbsprs)
- Use `Context#getSystemService` extension function. !249 [@tbsprs](https://gitlab.com/tbsprs)
- Replace deprecated `PreferenceManager`. !249 [@tbsprs](https://gitlab.com/tbsprs)
- Rename unused parameter. !249 [@tbsprs](https://gitlab.com/tbsprs)
- Use `SharedPreferences#edit` extension function. !249 [@tbsprs](https://gitlab.com/tbsprs)
- Allow using low-resolution pictures !224 [@alex.simm](https://gitlab.com/alex.simm)
- Make error logging in sentry optional !225 [@alex.simm](https://gitlab.com/alex.simm)
- Show distance for baskets based on the phone's location or profile address !204 [@alex.simm](https://gitlab.com/alex.simm)
- Replace the Picasso library with Glide for image loading !234 [@alex.simm](https://gitlab.com/alex.simm)
- Updated the main tab layout with tabs at the bottom !235 [@alex.simm](https://gitlab.com/alex.simm)
- Refactored the creation of baskets to MVVM #91 !236 [@alex.simm](https://gitlab.com/alex.simm)
- Refactored editing baskets to MVVM #91 !237 [@alex.simm](https://gitlab.com/alex.simm)
- Added a "give" tab that lists nearby food-share-points in cards !240 [@alex.simm](https://gitlab.com/alex.simm)
- Changed the colours of the tab bar on the bottom !245 [@alex.simm](https://gitlab.com/alex.simm)
- Simplify and reduce the number of utility methods !258 [@alex.simm](https://gitlab.com/alex.simm)
- New app icon for the beta app #93 !264 [@alex.simm](https://gitlab.com/alex.simm)
- Changed the colours to the ones used on the website !266 [@alex.simm](https://gitlab.com/alex.simm)
- Moved the map to the drawer !267 [@alex.simm](https://gitlab.com/alex.simm)
- Split the baskets fragment into two separate lists !269 [@alex.simm](https://gitlab.com/alex.simm)
- Moved android sdk to version 31 !275 [@chriswalg](https://gitlab.com/chriswalg)

## [0.6.6] - 2021-02-04

### Fixed

- Too long store names are displayed now on two lines. #120 !220 [@TheSoulT](https://gitlab.com/TheSoulT)

## [0.6.5] - 2021-01-28

### Changed

### Fixed

- Sets the default scheduler for Retrofit to avoid that network requests are executed on the main thread !213 [@dthulke](https://gitlab.com/dthulke)
- Switched the map markers to the new REST endpoint !217 [@alex.simm](https://gitlab.com/alex.simm)
- Fix bug that happens when opening a new conversation without any previous messages #121 !217 [@alex.simm](https://gitlab.com/alex.simm)

## [0.6.4] - 2020-05-16

### Added

### Changed

- Renamed variables and class from FairSharePoint to FoodSharePoint !210 [@alex.simm](https://gitlab.com/alex.simm)

### Fixed

- Avoid that the map is centered at the 0,0 coordinates !211 [@dthulke](https://gitlab.com/dthulke)
- Fixes a few messages parsing issues and prepares for changes in the ws server !209 [@dthulke](https://gitlab.com/dthulke)
- Updated different dependencies and fixed a few minor bugs !212 [@dthulke](https://gitlab.com/dthulke)

## [0.6.3] - 2020-05-09

### Added

### Changed

### Fixed

- Improves the validation of ws messages and converts messages from old server instances into the expected format !208 [@dthulke](https://gitlab.com/dthulke)

## [0.6.2] - 2020-05-08

### Fixed

- Adapts the app to the new conversation API in the backend !207 [@dthulke](https://gitlab.com/dthulke)

## [0.6.1] - 2020-04-24

### Added

- Show posts for profiles, send new posts, delete posts #97 !201 [@pascal-623](https://gitlab.com/pascal-623)
- Show posts for fair-share-points #97 !193 [@pascal-623](https://gitlab.com/pascal-623)
- New button for opening the website of foodsharing #109 !197 [@TheSoulT](https://gitlab.com/thesoult)
- Displays picture of baskets in list of baskets #108 !181 [@Srishti71](https://gitlab.com/Srishti71) [@linini](https://gitlab.com/linini)
- Allows to change picture of basket after basket already exists #25 !182 [@Srishti71](https://gitlab.com/Srishti71) [@linini](https://gitlab.com/linini)
- Allow to create and withdraw basket requests #71 !177 [@dthulke](https://gitlab.com/dthulke)
- New button for share/recommend the foodsharing app to other people #106 !185 [@TheSoulT](https://gitlab.com/thesoult)

### Changed

- Changed text, which is recommended by the recommend-Button !199 !203 [@TheSoulT](https://gitlab.com/thesoult)
- Changed title of "pick basket place menu" #100 !186 [@TheSoulT](https://gitlab.com/thesoult)
- Refactored conversation view to MVVM #91 !175 [@dthulke](https://gitlab.com/dthulke)
- Refactored user list view to MVVM #91 !176 [@dthulke](https://gitlab.com/dthulke)
- Allows to add a picture of a basket from gallery #49 !180 [@Srishti71](https://gitlab.com/Srishti71) [@linini](https://gitlab.com/linini)
- Allows users to see distance to basket from current (phone's) location #76 #54 !179 [@Srishti71](https://gitlab.com/Srishti71) [@linini](https://gitlab.com/linini)
- Updated map markers to be consistent with web #78 !183 [@CeciliaHwang](https://gitlab.com/CeciliaHwang)
- Tweaked the clustering of the markers on the map to avoid overlapping markers !190 [@dthulke](https://gitlab.com/dthulke)
- Moved all settings to the PreferenceManager !196 [@alex.simm](https://gitlab.com/alex.simm)
- Changes scroll behaviour of conversations list !192 [@dthulke](https://gitlab.com/dthulke)
- Require ViewModels to provide an error handler in request calls !205 [@dthulke](https://gitlab.com/dthulke)
- Improves error reporting !206 [@dthulke](https://gitlab.com/dthulke)
- Change the user API and model classes to fit to the backend changes !199 [@alex.simm](https://gitlab.com/alex.simm)

### Fixed

- Disabled zoom rounding in the map to avoid glitches !188 [@dthulke](https://gitlab.com/dthulke)
- Conversation are marked as unread again after opening them #111 !191 [@dthulke](https://gitlab.com/dthulke)
- Fixes ktlint !187 [@dthulke](https://gitlab.com/dthulke)
- Removes the "Distance" hint text in the distance text field in the basket activity !204 [@dthulke](https://gitlab.com/dthulke)

## [0.5.1] - 2019-09-22

### Added

### Changed

- Allows to open fair-share point and basket images in the picture activity and adds zoom and pan support to it #17 !172 [@dthulke](https://gitlab.com/dthulke)

### Fixed

- Fixed a bug which caused that the request section of a basket was not shown !173 [@alex.simm](https://gitlab.com/alex.simm)

## [0.5.0] - 2019-09-13

### Added

- Allow editing a basket's location #25 !146 [@alex.simm](https://gitlab.com/alex.simm)
- View for fair share points #30 !150 [@alex.simm](https://gitlab.com/alex.simm)
- Allow links to fair-share points to be opened in the app #63 !151 [@alex.simm](https://gitlab.com/alex.simm)
- Allow opening overlapping markers on the map #67 !163 [@dthulke](https://gitlab.com/dthulke)
- Shows the location of the user on the map #73 !164 [@dthulke](https://gitlab.com/dthulke)

### Changed

- Refactored profiles view to MVVM #91 !149 [@alex.simm](https://gitlab.com/alex.simm)
- Clarifies the labels of the basket lifetime picker #95 !161 [@dthulke](https://gitlab.com/dthulke)

### Fixed

- Fixed a bug which caused that long messages were not displayed correctly on some devices #68 !169 [@dthulke](https://gitlab.com/dthulke)

## [0.4.0] - 2019-08-14

### Added

- Link to the website for profiles and baskets #85 !125 [@alex.simm](https://gitlab.com/alex.simm)

### Changed

- Moved basket photo logic to PictureFragment class !127 [@alex.simm](https://gitlab.com/alex.simm)
- Reduced map zoom for baskets #80 !138 [@alex.simm](https://gitlab.com/alex.simm)

### Fixed

- Messages of jumpers are now shown in store conversation !130 [@dthulke](https://gitlab.com/dthulke)

## [0.3.1] - 2019-07-01

### Added

- User profiles with contact button #36 !104 [@alex.simm](https://gitlab.com/alex.simm)
- List of group chat members #47 !110 [@alex.simm](https://gitlab.com/alex.simm)

### Changed

### Fixed

## [0.3.0] - 2019-06-20

### Added

- Possibility to load more conversations #34 !59 [@alex.simm](https://gitlab.com/alex.simm)
- Map with the basket's location in the basket detail view !68 [@dthulke](https://gitlab.com/dthulke)
- Accept pictures from other apps to create a basket #49 !70 [@alex.simm](https://gitlab.com/alex.simm)
- A copy and a share url button to the basket detail view #39 !75 [@dthulke](https://gitlab.com/dthulke)
- Links to baskets can be opened via the app #63 !87 [@dthulke](https://gitlab.com/dthulke)
- Loading indicator for basket picture upload #61 !104 [@alex.simm](https://gitlab.com/alex.simm)

### Changed

- Improved the behaviour of the my location button #64 !88 [@dthulke](https://gitlab.com/dthulke)
- Possiblity to only show baskets or fair share points on the map #66 !89 [@dthulke](https://gitlab.com/dthulke)

### Fixed

- Fixed landscape mode for login screen #24 !57 [@alex.simm](https://gitlab.com/alex.simm)
- Fixes flickering and sometimes broken group profile images !63 [@dthulke](https://gitlab.com/dthulke)
- Reduced the size of the app #11 !65 [@dthulke](https://gitlab.com/dthulke)
- Basket picture in the NewBasketActivity is restored after configuration change !67 [@dthulke](https://gitlab.com/dthulke)
- Fixed a crash after aborting taking a picture while creating a basket #46 !73 [@dthulke](https://gitlab.com/dthulke)
- Styling of the buttons in dialogs !71 [@dthulke](https://gitlab.com/dthulke)
- Fixed line break problem in conversations #40 !79 [@alex.simm](https://gitlab.com/alex.simm)
- Use cache directory for temporary image file #60 !82 [@alex.simm](https://gitlab.com/alex.simm)
- Allow to scroll to the bottom of the new basket screen when the keyboard is open #74 [@dthulke](https://gitlab.com/dthulke)

## [0.2.0] - 2019-05-14

### Added

- Possibility to request baskets !44 [@dthulke](https://gitlab.com/dthulke)

### Fixed

- Using new logout endpoint #29 !52 [@alex.simm](https://gitlab.com/alex.simm)

## [0.1.0] - 2019-04-20

First public beta release!

### Fixed

- Downground okhttp to keep API 19 compataiblity
- Add null check to geo provider
- Fixed image picker crash
- Fixed tablayout styling
- Fixed support mail link

## [0.0.4] - 2019-04-11

### Added

- Add version name and extra links !40 [@dthulke](https://gitlab.com/dthulke)
- Map caching and cluster icons in Android 9 !36 [@dthulke](https://gitlab.com/dthulke)

### Fixed

- Fix unread message count #20 !38 [@dthulke](https://gitlab.com/dthulke)

### Removed

- Remove example credentials from login form #22 !39 [@dthulke](https://gitlab.com/dthulke)

## [0.0.3] - 2019-04-01

### Added

- Possibility to delete baskets #5 !31 [@alex.simm](https://gitlab.com/alex.simm).
- Zoom to current location on the map #12 !32 [@alex.simm](https://gitlab.com/alex.simm).

### Changed

- Loading all images via OkHttp and using caching #5 !31 [@alex.simm](https://gitlab.com/alex.simm).

## [0.0.2] - 2019-02-23

### Added

- nothing

## [0.0.1] - 2019-02-23

### Added

- everything
